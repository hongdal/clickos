#!/bin/bash

date +%s.%N
xl create /home/clickos/minios/click0.xen
DOMID=$(xl list | grep click0 | awk -F' ' '{print $2}')
echo "Domain ID: "$DOMID

/home/cosmos/dist/bin/cosmos start $DOMID /home/clickos/minios/ids.click

xl console $DOMID



