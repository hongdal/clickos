#!/bin/bash

counter=0
while [[ $counter -lt $1 ]]; do 
    let "counter=$counter+1";
    python /local/work/clickos/nfv/pypsender/pymainsender.py file /local/work/clickos/nfv/pypsender/dummyrules.cfg > /dev/null 2>&1 
done

python /local/work/clickos/nfv/pypsender/pymainsender.py file /local/work/clickos/nfv/pypsender/rules.cfg  > /dev/null 2>&1 

