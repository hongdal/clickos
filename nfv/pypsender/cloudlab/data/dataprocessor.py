__author__ = 'zhizhong pan'


import os
import numpy as np
import matplotlib as mpl
import matplotlib.pyplot as plt
import matplotlib.cm as cm
import operator as o

work_dir = os.getcwd()


def parse_data(file_name):
    all_data = list()
    with open(file_name) as data_file:
        for data_line in data_file:
            memory_size, rule_count, throughput = map(float, (data_line.strip().split(',')))
            if throughput < 20:
                throughput *= 1024
            all_data.append([int(memory_size), int(rule_count), int(throughput)])

    return all_data


def data_filter(memory_size_list, data):
    filtered_data = []
    for memory_size in memory_size_list:
        filtered_data += [i for i in data if i[0] == memory_size]

    return filtered_data

def plot_data(data):
    data_array = np.array(data)

    conditions = [i for i in np.unique(data_array[:, 0])]
    categories = [i for i in np.unique(data_array[:, 1])]

    data_array = np.array(sorted(data_array, key=lambda x: categories.index(x[1])))

    print(data_array)

    mpl.rc('font', family='sans-serif')
    mpl.rc('font', serif='Helvetica Neue')
    fig = plt.figure(figsize=(12, 6))
    axis = fig.add_subplot(111)
    for i, condition in enumerate(conditions):
        axis.plot(categories, data_array[data_array[:, 0] == condition][:, 2], linewidth=2, label=str(condition) + ' Mb',
                  color=cm.Dark2(float(i) / len(conditions)))

    axis.set_ylabel("Throughput (Mb/s) ", fontsize=14)
    axis.set_xlabel("Number of Rules  in a Virtual Firewall", fontsize=14)

    handles, labels = axis.get_legend_handles_labels()
    axis.legend(handles[::-1], labels[::-1], loc='upper right', title='Throughput')
    plt.show()


if __name__ == '__main__':
    data_list = parse_data(work_dir + '/outputdata.csv')
    data = data_filter([32, 64, 128, 256], data_list)
    plot_data(data)

