#!/bin/bash

if [[ -z $1 ]]; then 
    echo "Usage: $0 <firewall No.>"
    echo "e.g. $0 1"
    exit 1
fi

python pymainsender.py notify $1

