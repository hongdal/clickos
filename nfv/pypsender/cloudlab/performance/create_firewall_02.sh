#!/bin/bash

date
# create firewall_02 
xl create firewall_02.xen
sleep 0.1
# install click element
/local/work/cosmos/dist/bin/cosmos start firewall_02 firewall_02.click
sleep 0.1

# migrate traffic 
date 
/local/work/clickos/nfv/orchestration/flowsender/migration.sh 1 2 > /dev/null
date 


echo "----------------------------------------"
# install rules (10+35=45)
./pytester.py file install_firewall_02.cfg 1
date 
# activation 
./activation.sh 2 
date 

# remove old rules (10+35=45)
./pytester.py file remove_firewall_01.cfg 1


