#!/bin/bash

rule_num=(1800 1700 1600 1500 1400 1300 1200 1100 1000 900 800 700 600 500 400 300 200 100 0) 
memory_size=(16 32 64 128 256 512 1024 1536 2048)

for size in ${memory_size[*]}; do 
    # destroy instances.
    xl destroy firewall_01 > /dev/null 2>&1
    sleep 1
    xl destroy firewall_02 > /dev/null 2>&1
    sleep 1
    # destroy and create new instance.
    sed -r -i "s/memory = '[0-9]+'/memory = '$size'/" /local/work/set-up-clickos/exps/firewall_01.xen   
    sed -r -i "s/memory = '[0-9]+'/memory = '$size'/" /local/work/set-up-clickos/exps/firewall_02.xen  
    xl create /local/work/set-up-clickos/exps/firewall_01.xen > /dev/null 2>&1
    sleep 1
    cosmos start firewall_01 /local/work/set-up-clickos/exps/firewall_01.click > /dev/null 2>&1 
    sleep 1
    xl create /local/work/set-up-clickos/exps/firewall_02.xen > /dev/null 2>&1
    sleep 1
    cosmos start firewall_02 /local/work/set-up-clickos/exps/firewall_02.click > /dev/null 2>&1 
    sleep 5
    #  
    for num in ${rule_num[*]}; do 
        printf "$size,$num,"
        # clear all rules.
        python pymainsender.py file clear_rule.cfg > /dev/null 2>&1 
        # send firewall policies. 
        ./batchsender.sh `expr $num / 10` > /dev/null 2>&1
        # start
        ssh hongdal@10.10.1.1 'iperf -c 10.10.1.2 -w 400K' | grep 'bits/sec' | awk -F ' ' '{print $7}' 
        # come back. got to next iteration.  
    done
done



