#!/bin/bash

if [[ -z "$1" ]]; then 
    echo "Usage: $0 <number of rules / $(expr `cat dummyrules.cfg | wc -l ` / 2)> "
    exit 1
fi
counter=0
while [[ $counter -lt $1 ]]; do 
    let "counter=$counter+1";
    python pymainsender.py file dummyrules.cfg > /dev/null 2>&1 
done
python pymainsender.py file rules.cfg  > /dev/null 2>&1 

