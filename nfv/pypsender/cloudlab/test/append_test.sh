#!/bin/bash
# This script tests the time comsumed by installing 50, 100, ... 1000 firewall
# rules.

index=0
count=$1
if [[ -z $count ]]; then 
    echo "Usage: $0 <number of rules> "
    exit 1
fi

filename=$count"_append.cfg"
repeat=`expr $1 / 45`
if [[ `expr $1 % 45`  -ne 0 ]]; then 
    repeat=$(($repeat+1))
fi 

old=`date +%s.%N`
# send out rules. 
#./pytester.py file $filename 1 > /dev/null 2>&1
./pytester.py file "45_append.cfg" $repeat > /dev/null 2>&1
new=`date +%s.%N`

old_sec=`echo $old | cut -d '.' -f1`
old_nsec=`echo $old | cut -d '.' -f2`
new_sec=`echo $new | cut -d '.' -f1`
new_nsec=`echo $new | cut -d '.' -f2`
dms_sec=`expr $(expr $new_sec \* 1000) - $(expr $old_sec \* 1000)`
dms_nsec=`expr $(expr $new_nsec / 1000000) - $(expr $old_nsec / 1000000)`
echo $count","`expr $dms_sec + $dms_nsec`
./reset.sh > /dev/null 2>&1



