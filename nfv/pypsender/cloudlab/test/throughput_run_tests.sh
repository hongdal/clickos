#!/bin/bash
# This script run evaluations for vairous number of firewall rules.
# I.e. 0, 50, 100, 150, ... 500. 

if [[ -z $2 ]]; then 
    echo "Usage: $0 <steps> <step> [repeat]"
    echo "E.g., $0 10 50 100 --> Will test for 0, 50, 100, ... 500 rules"
    exit 1
fi 

max=`expr 1 + $(expr $1 \* $2)`
index=550
repeat=$3
if [[ -z $repeat ]]; then 
    repeat=50
fi

while [[ $index -lt $max ]]; do 
    ./throughput_one_test.sh $index $repeat 
    index=$(($index+$2))
done


