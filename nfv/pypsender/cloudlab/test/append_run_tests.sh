#!/bin/bash

if [[ -z $2 ]]; then 
    echo "Usage: $0 <RANGE> <STEP> [repeats]"
    echo "<RANGE> is a number indicating from STEP to STEP*RANGE rules will be tested"
    exit 1
fi

max=`expr 1 + $(expr $2 \* $1)`
index=0
repeat=$3
if [[ -z $repeat ]]; then 
    repeat=10
fi

while [[ $index -lt $max ]]; do 
    index=$(($index+$2))
    echo "Run test for $index number of rules ..."
    ./append_one_test.sh $index $repeat
done

