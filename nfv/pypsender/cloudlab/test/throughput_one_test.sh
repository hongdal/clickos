#!/bin/bash
# This script evaluate the throughput of a certain number of firewall rules
# for multiple times. Each time may take 13~15 seconds. 

if [[ -z $1 ]]; then 
    echo "Usage: $0 <number of rules> [repeat]"
    echo "E.g., $0 500 100 --> install 500 rules and repeat for 100 times."
    exit 1
fi

count=$1
repeat=$2
if [[ -z $repeat ]]; then 
    repeat=50 
fi

# clear all. 
./reset.sh > /dev/null 2>&1
# install 1000 dummy rules. 
./throughput_insdummy.sh $count > /dev/null 2>&1
# install appropriate rules.
./pytester.py file rule_tcp.cfg 1 > /dev/null 2>&1

sleep "2"

rm -f "./results/throughput."$count".txt" > /dev/null 2>&1
index=0
while [[ $index -lt $repeat ]]; do
    ./throughput_test.sh >> "./results/throughput."$count".txt"
    let "index=$index+1"
    echo "COMPLETION: $index times of $count"
done

# clear all rules.
./reset.sh > /dev/null 2>&1
