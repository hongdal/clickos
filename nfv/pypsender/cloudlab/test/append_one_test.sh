#!/bin/bash

if [[ -z $1 ]]; then 
    echo "Usage: $0 <number of rules> [repeat]"
    echo "E.g., $0 500 100 --> send 500  rules, and repeat 100 times"
    exit 1
fi

count=$1
repeat=$2
if [[ -z $repeat ]]; then 
    repeat=100 
fi
rm -f "./results/append."$count.txt

index=0
while [[ $index -lt $repeat ]]; do 
    ./append_test.sh $count >> "./results/append."$count.txt
    let "index=$index+1"
done


