#!/bin/bash
# This script tests the time comsumed by removing some number of firewall 
# rules.

index=0
count=$1
if [[ -z $count ]]; then 
    echo "Usage: $0 <number of rules> "
    echo "Number of rules must be 10*i."
    exit 1
fi

repeats=`expr $count / 45`
old=`date +%s.%N`
# send out rules. 
./pytester.py file 45_delete.cfg $repeats > /dev/null 2>&1
new=`date +%s.%N`

old_sec=`echo $old | cut -d '.' -f1`
old_nsec=`echo $old | cut -d '.' -f2`
new_sec=`echo $new | cut -d '.' -f1`
new_nsec=`echo $new | cut -d '.' -f2`
dms_sec=`expr $(expr $new_sec \* 1000) - $(expr $old_sec \* 1000)`
dms_nsec=`expr $(expr $new_nsec / 1000000) - $(expr $old_nsec / 1000000)`
echo $count","`expr $dms_sec + $dms_nsec`



