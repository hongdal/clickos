#!/bin/bash

if [[ -z $1 ]]; then 
    echo "Usage: $0 <number of rules> [repeat]"
    echo "E.g., $0 500 100 --> remove 500 rules, and repeat 100 times"
    exit 1
fi

count=$1
repeat=$2
if [[ -z $repeat ]]; then 
    repeat=100 
fi
rm -f "./results/remove."$count.txt

index=0
while [[ $index -lt $repeat ]]; do 
    ./remove_refule.sh $count > /dev/null 2>&1
    ./remove_test.sh $count >> "./results/remove."$count.txt
    let "index=$index+1"
    # refule. 
done


