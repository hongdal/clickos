#!/bin/bash
if [[ -z $1 ]]; then 
    echo "Usage: $0 <rule number>"
    echo "E.g., $0 1000 --> append 1000 rules at the end of the table."
    exit 1
fi

repeat=`expr $1 / 10`
./pytester.py file 10_dummy.cfg $repeat

