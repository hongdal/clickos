#!/bin/bash

files=$(ls -l remove.*.txt | awk '{print $9}' | sort -t '.' -k 2 -n)
echo "rules,average,variance"
for f in $files; do 
    fname=`echo $f | cut -d '.' -f 2`
    ret=`awk -F ',' 'BEGIN{sum=0;summ=0}{sum+=$2;summ+=$2*$2}END{avg=sum/NR;avg2=summ/NR;printf("%f,%f",avg,sqrt(avg2-avg*avg))}' $f`
    echo "$fname,$ret"
done
