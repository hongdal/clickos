#!/bin/bash

if [[ -z $2 ]]; then 
    echo "Usage: $0 <RANGE> <STEP> [repeats]"
    echo "<RANGE> is a number indicating from STEP to STEP*RANGE rules will be tested"
    exit 1
fi

max=`expr 1 + $(expr $2 \* $1)`
index=0
repeat=$3
if [[ -z $repeat ]]; then 
    repeat=100
fi

# fill 1000 rules. 
./remove_fill.sh 
if [[ $? -ne 0 ]]; then 
    echo "fill rules error."
    exit 1
fi

while [[ $index -lt $max ]]; do 
    index=$(($index+$1))
    ./remove_one_test.sh $index $repeat
    echo "COMPLETION: $index number of rules."
done

# clear all.
./reset.sh 

