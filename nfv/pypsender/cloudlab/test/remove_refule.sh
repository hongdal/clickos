#!/bin/bash
# Append number of rules at the end of the firewall table. 
# These appended rules is going to be removed next time for the purpose
# of testing.

if [[ -z $1 ]]; then 
    echo "Usage: $0 <number of rules>"
    exit 1
fi

repeat=`expr $1 / 45`
./pytester.py file 45_append.cfg $repeat

