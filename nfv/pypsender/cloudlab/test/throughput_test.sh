#!/bin/bash
# This script evaluate the throughput under a certain number of firewall rules.
# This script assumes that there is enough dummy rules in the firewall table. 
# It's job is to install appropriate rules and start an 'iperf' in the client. 
# Then it comes back with the 'iperf' output. 

# start iperf client. 
ssh hongdal@10.130.127.1 'iperf -c 10.130.127.2 -y C' | awk -F ',' '{printf "%d,%d,%d\n",$1,$8,$9}'






