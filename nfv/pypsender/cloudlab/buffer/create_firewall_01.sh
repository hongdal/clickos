#!/bin/bash

date +%s.%N
# create firewall_01 
xl create firewall_01.xen
sleep 0.1 

date +%s.%N
/local/work/cosmos/dist/bin/cosmos start firewall_01 firewall_01.click

# migrate traffic 
date +%s.%N
/local/work/clickos/nfv/orchestration/flowsender/migration.sh 2 1 > /dev/null
date +%s.%N

# init rules 
./pytester.py file install_firewall_01_1.cfg 10
date +%s.%N

# activation 
./activation.sh 1
date +%s.%N

# remove old rules (10+35=45)
./pytester.py file remove_firewall_02.cfg 10

