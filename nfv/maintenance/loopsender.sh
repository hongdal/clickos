#!/bin/bash

if [[ $# -ne 2 ]]; then
    echo "Usage: ./loopSender <xenbr number> <interval>"
    echo "e.g. ./loopSender 0 1 --> use xenbr0, every 1 second send one packet."
    exit 0
fi

brnum=$1
interval=$2

counter=0
while [[ 1 ]]; do
    if [[ $1 -eq 0 ]]; then 
        ./psender_xenbr00 0 -a 172.16.97.1,172.16.97.2,tcp,90,80,allow
    
    elif [[ $1 -eq 1 ]]; then
        ./psender_xenbr01 0 -a 172.16.97.1,172.16.97.2,tcp,90,80,allow 
    
    else
        echo "xenbr number must be 0 or 1"
        exit 0
    fi
    
    sleep $2
    let "counter=$counter+1"
    echo $counter
done






