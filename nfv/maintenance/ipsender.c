// Send an IPv4 test (with 0xfd as layer 4 protocol) packet via raw socket.
// Stack fills out layer 2 (data link) information (MAC addresses) for us.
/**
 *      This program reads configurations from files and construct a packet 
 *      to send out.
 *
 *      The maximum size of the packet's payload should be 500;
 *      If the configuration file is too large, two packets will be sent,
 *
 *
 * **/


#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>           // close()
#include <string.h>           // strcpy, memset(), and memcpy()

#include <netdb.h>            // struct addrinfo
#include <sys/types.h>        // needed for socket(), uint8_t, uint16_t, uint32_t
#include <sys/stat.h>
#include <fcntl.h>
#include <sys/socket.h>       // needed for socket()
#include <netinet/in.h>       // IPPROTO_RAW, IPPROTO_IP, INET_ADDRSTRLEN
#include <netinet/ip.h>       // struct ip and IP_MAXPACKET (which is 65535)
#include <arpa/inet.h>        // inet_pton() and inet_ntop()
#include <sys/ioctl.h>        // macro ioctl is defined
#include <bits/ioctls.h>      // defines values for argument "request" of ioctl.
#include <net/if.h>           // struct ifreq

#include <errno.h>            // errno, perror()

// Define some constants.
#define IP4_HDRLEN 20         // IPv4 header length
// Define the length of conent.
#define DATA_LENGTH 1400
// Define the max length of each rule. 
#define RULE_LENGTH 1300

enum OPTION_COMMANDS { C_APPEND, C_REPLACE };
enum OPTION_DEBUG { D_PRINT };

// Function prototypes
uint16_t checksum (uint16_t *, int);
char *allocate_strmem (int);
uint8_t *allocate_ustrmem (int);
int *allocate_intmem (int);


/***
 *  This structrue is defined to store a pasred rule. 
 *  The original rule is passed by user. 
 *
 ***/
struct rule {
    uint32_t src_ip;
    uint32_t dst_ip;
    uint16_t src_port;
    uint16_t dst_port;
    uint8_t protocol;
    uint8_t action;
};
#define RULE_SIZE 16


/**
 *  construct a rule, and fufill the give r.
 *  char* ruleString is considered as the input of the user. 
 *  index is considered as index.
 *
 *
 *  If Success, return nozero.
 *  If fail, return zero. 
 *  
 ***/
int feedRuleWithIndex(struct rule* r, char* ruleString, int index)
{
    int ret = 1;

    return ret;
}
/**
 *  construct a rule, and fufill the give r.
 *  char* ruleString is considered as the input of the user. 
 *
 *  If Success, return nozero.
 *  If fail, return zero. 
 *  
 ***/
int feedRule(struct rule* r, char* ruleString)
{
    int ret = 1;
    char protstr[20] = {0};
    char address[20] = {0};
    char *data = ruleString; 
    char *f1 = NULL;
    char *f2 = NULL;
    char *f3 = NULL;
    char *f4 = NULL;
    struct in_addr srcip, dstip;
    uint32_t src_ip;
    uint32_t dst_ip;
    uint16_t src_port;
    uint16_t dst_port;
    uint8_t protocol;
    uint8_t action;
     
    /* Resolve source ip. */ 
    f1 = strchr(data, ',');
    if ( NULL == f1 ) {
        goto BadRule;
    }
    memcpy(address, data, f1-data);
    address[f1-data] = 0;
    /* The inet_aton function returns ip address as network order. */
    if ( inet_aton(address, &srcip) == 0 ) {
        fprintf(stderr, "Could not resolve src ip address.\n");
        goto BadRule;
    }else{
        memcpy(&(r->src_ip), (uint8_t*)&(srcip.s_addr), sizeof(r->src_ip));
    }
    f1++;

    /* Resolve destination ip. */ 
    f2 = strchr(f1, ',');
    if ( NULL == f2 ) {
        goto BadRule;
    }
    memcpy(address, f1, f2-f1);
    address[f2-f1] = 0;
    /* The inet_aton function returns ip address as network order. */
    if ( inet_aton(address, &dstip) == 0 ) {
        fprintf(stderr, "Could not resolve dst ip address.\n");
        goto BadRule;
    }else{
        memcpy(&(r->dst_ip), (uint8_t*)&(dstip.s_addr), sizeof(r->dst_ip));
    }
    f2++;
    f1 = f2;
    
    /* Fill the protocol. */
    f2 = strchr(f1, ',');
    if ( NULL == f2 ) {
        goto BadRule;
    }
    memcpy(protstr, f1, f2-f1); 
    protstr[f2-f1] = 0;
    if ( strcmp(protstr, "tcp") == 0 ) {
        protocol = 6;
    }else if ( strcmp(protstr, "icmp") == 0 ) {
        protocol = 1;
    }else{
        fprintf(stderr, "Unknow protocol type!\n");
        goto BadRule;
    } 
    r->protocol = protocol; 

    /* Fill the prots. */
    /* prots number are in network order. */
    switch (protocol) {
        default: {
            printf("Unknow protocol type!\n");
            goto BadRule;
            break;
        }
        /* ICMP */
        case 1: {
            // port number is zero
            memset(&(r->src_port), 0, sizeof(r->src_port));
            memset(&(r->dst_port), 0, sizeof(r->dst_port));
            break;
        }
        /* TCP */
        case 6: {
            f2++;
            f3 = strchr(f2,',');
            if ( NULL == f3 ) {
                goto BadRule; 
            }
            memcpy(protstr, f2, f3-f2);
            if ( sscanf(protstr, "%d", &src_port) != 1 ) {
                goto BadRule;    
            }
            r->src_port = htons(src_port);
            f3++;
            f4 = strchr(f3,',');
            if ( NULL == f4 ) {
                goto BadRule; 
            }
            memcpy(protstr, f3, f4-f3);
            protstr[f4-f3] = 0;
            if ( sscanf(protstr, "%d", &dst_port) != 1 ) {
                goto BadRule;    
            }
            r->dst_port = htons(dst_port);
            break;
        }
    }

    /* Fill action. */ 
    f2 = strrchr(data, ',');
    if ( NULL == f2 ) {
        goto BadRule;
    }
    f2++;
    if ( strcmp(f2, "drop") == 0 ) {
        r->action = 0;
    } else if ( strcmp(f2, "allow") == 0 ) {
        r->action = 1;
    } else {
        fprintf(stderr, "Unknow action type.\n");
        goto BadRule;
    }


    return ret;

BadRule:
    fprintf(stderr, "Could not parse rule: \'$s\' \n", ruleString);
    ret = 0;
    return ret;
}

/***
 *  
 *  Given operation code, option and rule string, this function will
 *  fulfill the packet's payload accordingly. 
 *  Please note that the given rule string should be a legal rule. 
 *  This function will adjust the current offset of strbuf from the beginning 
 *  of packet payload. 
 *
 *  strbuf  : a point to packet payload.   
 *  argv    : rule string. 
 *  code    : 0 --> command,  1 --> debug
 *  option  : 1->append, 2->replace, 3->insert, 4->delete, 5->check.
 *  offset  : address, current offset of strbuf from the beginnig of packet payload. 
 *
 *
 *  return: 1 on success, 0 on failure
 *
 ***/
int parseAndFeed(char* strbuf, char *argv, uint8_t code, uint8_t option, char** offset)
{
    int ret = 1;
    struct rule r; 

    switch ( code ) 
    {
        defualt : {
            fprintf(stderr, "Unknow mode!\n");
            goto BadRule;
            break;
        }
        /* sending commands */
        case 0 : {
            switch ( option ) 
            {
                default : {
                    fprintf(stderr, "Unknow option type!\n");
                    goto BadRule;
                    break;
                }
                /* append */
                case 'a' : {
                    /* feed rule */
                    if ( feedRule(&r, argv) != 1 ) {
                        goto BadRule;
                    }
                    memcpy(strbuf, &r, RULE_SIZE);
                    /* insert delimiter */
                    strbuf[RULE_SIZE] = '\n';
                    /* adjust offset. */
                    *offset = strbuf+RULE_SIZE+1;
                    break;
                }
                /* insert */
                case 'i' : {
                    /* set index value. */ 
                    /* feed rule */
                    /* insert delimiter */
                    /* adjust offset. */
                    break;
                }
            }
            break;
        }
        /* sending bebug info */
        case 1 : {
            switch ( option ) 
            {
                default : {
                    fprintf(stderr, "Unknow option for debug mode. \n");
                    goto BadRule;
                }
                /* print firewall table. */
                case 'p' : {
                    /* do nothing */
                    break;
                }
            }
            break;
        }
    }
    return ret;

BadRule:
    ret = 0;
    return ret;
}




int
main (int argc, char **argv)
{
    int i, status, sd, *ip_flags;
    int slen = 100;    // length of the string
    char strbuf[DATA_LENGTH] = {0}; 
    char aline[RULE_LENGTH] = {0};
    const int on = 1;
    char *interface, *target, *src_ip, *dst_ip;
    struct ip iphdr;
    uint8_t *packet;
    struct addrinfo hints, *res;
    struct sockaddr_in *ipv4, sin;
    struct ifreq ifr;
    void *tmp;
    FILE *fp = NULL;
    size_t fsize;
    int code;
    char option;
    /* record current payload offset. */
    char* offset; 
    /* count current rule. */
    uint16_t ruleCount = 0;
    size_t rule_config_length = 0;

    if ( argc < 3 )
    {
        printf("Usage: ./psender <code> [options] [contents]\n");
        printf("e.g.  ./psender  0 -a \"172.16.97.80,172.16.97.81,tcp,*,80,deney\" \n");
        exit (EXIT_FAILURE);
    }
    if ( sscanf(argv[1], "%d", &code) != 1 )
    {
        fprintf(stderr, "Unknow code!\n");
        exit (EXIT_FAILURE);
    }
    
    /* feed the first serval byts. */
    strbuf[0] = code;
    if ( sscanf(argv[2], "-%c", &option) != 1 )
    {
        fprintf(stderr, "Bad options!\n");
        exit (EXIT_FAILURE);
    } 
    /*** 
     * payload offset is set to strbuf+2. 
     * strbuf               --> code,
     * strbuf+1             --> option, 
     * strbuf+2, strbur+3   --> number of rules involved. 
    ***/
    offset = strbuf+4;

    switch (code)
    {
        /* send commands */
        case 0 : {
            /* feed option */
            switch ( option ) {
                default : {
                    fprintf(stderr, "Unknow option type!\n");
                    exit (EXIT_FAILURE);
                }
                case 'a' : {
                    strbuf[1] = (uint8_t)C_APPEND;
                    break;
                }
            }
            /* If it's a command, it must be followed by a file name */
            if ( argc < 4 ) {
                fprintf(stderr, "Hope 3 arguments, but only %d is give!", argc-1);
                exit (EXIT_FAILURE);
            }
            /* fetch rules from configuration file.  */
            fp = fopen(argv[3], "rb");
            if ( NULL == fp ) {
                fprintf(stderr, "Could not find rule configuration file!\n");
                exit (EXIT_FAILURE);
            }
            fseek(fp, 0, SEEK_END);
            rule_config_length = ftell(fp);
            fseek(fp, 0, SEEK_SET);
            if ( RULE_LENGTH < rule_config_length ) {
                rule_config_length = RULE_LENGTH;
            }
            if ( fread(aline, sizeof(char), rule_config_length, fp) != rule_config_length ) {
                fprintf(stderr, "Read from rule configuration file error!\n");
                exit (EXIT_FAILURE);
            }
            ruleCount = 0; 
            char *ruleEnd = aline;
            char *ruleStart;

            // for each rule, append it to the tail of the packet. 
            for ( ruleStart = ruleEnd; (ruleEnd = strchr(ruleEnd, '\n')) != NULL; ruleEnd++ )
            {
                if ( 0 == ruleEnd-ruleStart ) {
                    ruleStart = ruleEnd+1;
                    continue;
                }
                char* ruleEntry = malloc(sizeof(char)*(ruleEnd-ruleStart)+1);
                memcpy(ruleEntry,ruleStart, sizeof(char)*(ruleEnd-ruleStart));
                ruleEntry[sizeof(char)*(ruleEnd-ruleStart)] = 0; 
                
                /* call paser to feed the content. */
                if ( parseAndFeed(offset, ruleEntry, code, option, &offset) != 1 ) {
                    fprintf(stderr, "Parse command error!\n"); 
                    exit (EXIT_FAILURE);
                }
                free(ruleEntry);
                ruleStart = ruleEnd+1;
                ruleCount++;
            }
            /* 255 rules maximun. */
            ruleCount = htons(ruleCount);
            memcpy(strbuf+2, &(ruleCount), sizeof(uint16_t));
            /*****************************/
            printf("%d rules have been added!\n", ruleCount);
            break;
        }
        
        /*sending debug info */
        case 1 : {
            switch ( option )
            {
                /* print firewall tables */
                case 'p' : {
                    strbuf[1] = (uint8_t)D_PRINT;
                    if ( parseAndFeed(offset, NULL, code, option, &offset) != 1 ) {
                        fprintf(stderr, "Parse debug message error!\n"); 
                        exit (EXIT_FAILURE);
                    }
                    break;   
                }
            }
            break;
        }
    }
   
    printf("src_port= %hu\n", ntohs(*(uint16_t*)(strbuf+8)));
    printf("dst_port= %hu\n", ntohs(*(uint16_t*)(strbuf+10)));
    //slen = strlen(strbuf); 
    //slen = slen > DATA_LENGTH ? DATA_LENGTH : slen;
    slen = DATA_LENGTH;
    // Allocate memory for various arrays.
    packet = allocate_ustrmem (IP_MAXPACKET);
    interface = allocate_strmem (40);
    target = allocate_strmem (40);
    src_ip = allocate_strmem (INET_ADDRSTRLEN);
    dst_ip = allocate_strmem (INET_ADDRSTRLEN);
    ip_flags = allocate_intmem (4);
    //strbuf = allocate_strmem (slen+1);

    // read src_ip and dst_ip from file.
    fp = fopen("./sender.cfg", "rb");
    if ( NULL == fp ) {
        printf("Could Not find sender.cfg file. use default values for ip addresses.\n");
        // Interface to send packet through.
        strcpy(interface, "xenbr01");
        // Source IPv4 address: you need to fill this out
        strcpy (src_ip, "172.16.97.79");
        // Destination URL or IPv4 address: you need to fill this out
        strcpy (target, "172.16.97.82");
    } else {
        //fgets(interface, 40, fp);
        fscanf(fp, "%s", interface);
        //interface[strlen(interface)-1] = 0;
        //fgets(src_ip, INET_ADDRSTRLEN, fp);
        fscanf(fp, "%s", src_ip);
        //src_ip[strlen(src_ip)-1] = 0;
        //fgets(target, 40, fp);
        fscanf(fp, "%s", target);
        //target[strlen(target)-1] = 0;
        printf("interface: %s\n", interface);
        printf("src: %s\n", src_ip);
        printf("target: %s\n", target);
    }

    // feed the string 
    //memcpy(strbuf, argv[1], slen);
    //*(strbuf+slen) = 0x00;

    // Submit request for a socket descriptor to look up interface.
    if ((sd = socket (AF_INET, SOCK_RAW, IPPROTO_RAW)) < 0) {
        perror ("socket() failed to get socket descriptor for using ioctl() ");
        exit (EXIT_FAILURE);
    }

    // Use ioctl() to look up interface index which we will use to
    // bind socket descriptor sd to specified interface with setsockopt() since
    // none of the other arguments of sendto() specify which interface to use.
    memset (&ifr, 0, sizeof (ifr));
    snprintf (ifr.ifr_name, sizeof (ifr.ifr_name), "%s", interface);
    if (ioctl (sd, SIOCGIFINDEX, &ifr) < 0) {
        perror ("ioctl() failed to find interface ");
        return (EXIT_FAILURE);
    }
    close (sd);
    printf ("Index for interface %s is %i\n", interface, ifr.ifr_ifindex);


    // Fill out hints for getaddrinfo().
    memset (&hints, 0, sizeof (struct addrinfo));
    hints.ai_family = AF_INET;
    hints.ai_socktype = SOCK_STREAM;
    hints.ai_flags = hints.ai_flags | AI_CANONNAME;

    // Resolve target using getaddrinfo().
    if ((status = getaddrinfo (target, NULL, &hints, &res)) != 0) {
        fprintf (stderr, "getaddrinfo() failed: %s\n", gai_strerror (status));
        exit (EXIT_FAILURE);
    }
    ipv4 = (struct sockaddr_in *) res->ai_addr;
    tmp = &(ipv4->sin_addr);
    if (inet_ntop (AF_INET, tmp, dst_ip, INET_ADDRSTRLEN) == NULL) {
        status = errno;
        fprintf (stderr, "inet_ntop() failed.\nError message: %s", strerror (status));
        exit (EXIT_FAILURE);
    }
    freeaddrinfo (res);

    // IPv4 header

    // IPv4 header length (4 bits): Number of 32-bit words in header = 5
    iphdr.ip_hl = IP4_HDRLEN / sizeof (uint32_t);

    // Internet Protocol version (4 bits): IPv4
    iphdr.ip_v = 4;

    // Type of service (8 bits)
    iphdr.ip_tos = 0;

    // Total length of datagram (16 bits): IP header + strlen
    iphdr.ip_len = htons (IP4_HDRLEN + slen);

    // ID sequence number (16 bits): unused, since single datagram
    iphdr.ip_id = htons (0);

    // Flags, and Fragmentation offset (3, 13 bits): 0 since single datagram

    // Zero (1 bit)
    ip_flags[0] = 0;

    // Do not fragment flag (1 bit)
    ip_flags[1] = 0;

    // More fragments following flag (1 bit)
    ip_flags[2] = 0;

    // Fragmentation offset (13 bits)
    ip_flags[3] = 0;

    iphdr.ip_off = htons ((ip_flags[0] << 15)
                          + (ip_flags[1] << 14)
                          + (ip_flags[2] << 13)
                          +  ip_flags[3]);

    // Time-to-Live (8 bits): default to maximum value
    iphdr.ip_ttl = 255;

    // Transport layer protocol (8 bits): 0xfd for testing
    iphdr.ip_p = 0x0fd;

    // Source IPv4 address (32 bits)
    if ((status = inet_pton (AF_INET, src_ip, &(iphdr.ip_src))) != 1) {
        fprintf (stderr, "inet_pton() failed.\nError message: %s", strerror (status));
        exit (EXIT_FAILURE);
    }

    // Destination IPv4 address (32 bits)
    if ((status = inet_pton (AF_INET, dst_ip, &(iphdr.ip_dst))) != 1) {
        fprintf (stderr, "inet_pton() failed.\nError message: %s", strerror (status));
        exit (EXIT_FAILURE);
    }

    // IPv4 header checksum (16 bits): set to 0 when calculating checksum
    iphdr.ip_sum = 0;
    iphdr.ip_sum = checksum ((uint16_t *) &iphdr, IP4_HDRLEN);

/********************************/
    // Prepare packet.

    // First part is an IPv4 header.
    memcpy (packet, &iphdr, IP4_HDRLEN * sizeof (uint8_t));

    /*  copy string that we need to print into the packet.  */
    memcpy ((packet + IP4_HDRLEN), strbuf, slen);

    // The kernel is going to prepare layer 2 information (ethernet frame header) for us.
    // For that, we need to specify a destination for the kernel in order for it
    // to decide where to send the raw datagram. We fill in a struct in_addr with
    // the desired destination IP address, and pass this structure to the sendto() function.
    memset (&sin, 0, sizeof (struct sockaddr_in));
    sin.sin_family = AF_INET;
    sin.sin_addr.s_addr = iphdr.ip_dst.s_addr;


    // Submit request for a raw socket descriptor.
    if ((sd = socket (AF_INET, SOCK_RAW, IPPROTO_RAW)) < 0) {
        perror ("socket() failed ");
        exit (EXIT_FAILURE);
    }

    // Set flag so socket expects us to provide IPv4 header.
    // set IP_HDRINCL, so we can use our ip header.
    if (setsockopt (sd, IPPROTO_IP, IP_HDRINCL, &on, sizeof (on)) < 0) {
        perror ("setsockopt() failed to set IP_HDRINCL ");
        exit (EXIT_FAILURE);
    }

    // Bind socket to interface index.
    if (setsockopt (sd, SOL_SOCKET, SO_BINDTODEVICE, &ifr, sizeof (ifr)) < 0) {
        perror ("setsockopt() failed to bind to interface ");
        exit (EXIT_FAILURE);
    }

    // Send packet.
    // sendto (socket, buff, length, flag, struct sockaddr*, length)
    if (sendto (sd, packet, IP4_HDRLEN + slen, 0, (struct sockaddr *) &sin, sizeof (struct sockaddr)) < 0)  {
        perror ("sendto() failed ");
        exit (EXIT_FAILURE);
    }

    // Close socket descriptor.
    close (sd);

    // Free allocated memory.
    free (packet);
    free (interface);
    free (target);
    free (src_ip);
    free (dst_ip);
    free (ip_flags);
    //free (strbuf);

    return (EXIT_SUCCESS);
}

// Computing the internet checksum (RFC 1071).
uint16_t
checksum (uint16_t *addr, int len)
{
    int count = len;
    register uint32_t sum = 0;
    uint16_t answer = 0;

    // Sum up 2-byte values until none or only one byte left.
    while (count > 1) {
        sum += *(addr++);
        count -= 2;
    }

    // Add left-over byte, if any.
    if (count > 0) {
        sum += *(uint8_t *) addr;
    }

    // Fold 32-bit sum into 16 bits; we lose information by doing this,
    // increasing the chances of a collision.
    // sum = (lower 16 bits) + (upper 16 bits shifted right 16 bits)
    while (sum >> 16) {
        sum = (sum & 0xffff) + (sum >> 16);
    }

    // Checksum is one's compliment of sum.
    answer = ~sum;

    return (answer);
}


// Allocate memory for an array of chars.
char *
allocate_strmem (int len)
{
    void *tmp;

    if (len <= 0) {
        fprintf (stderr, "ERROR: Cannot allocate memory because len = %i in allocate_strmem().\n", len);
        exit (EXIT_FAILURE);
    }

    tmp = (char *) malloc (len * sizeof (char));
    if (tmp != NULL) {
        memset (tmp, 0, len * sizeof (char));
        return (tmp);
    } else {
        fprintf (stderr, "ERROR: Cannot allocate memory for array allocate_strmem().\n");
        exit (EXIT_FAILURE);
    }
}

// Allocate memory for an array of unsigned chars.
uint8_t *
allocate_ustrmem (int len)
{
    void *tmp;

    if (len <= 0) {
        fprintf (stderr, "ERROR: Cannot allocate memory because len = %i in allocate_ustrmem().\n", len);
        exit (EXIT_FAILURE);
    }

    tmp = (uint8_t *) malloc (len * sizeof (uint8_t));
    if (tmp != NULL) {
        memset (tmp, 0, len * sizeof (uint8_t));
        return (tmp);
    } else {
        fprintf (stderr, "ERROR: Cannot allocate memory for array allocate_ustrmem().\n");
        exit (EXIT_FAILURE);
    }
}

// Allocate memory for an array of ints.
int *
allocate_intmem (int len)
{
    void *tmp;

    if (len <= 0) {
        fprintf (stderr, "ERROR: Cannot allocate memory because len = %i in allocate_intmem().\n", len);
        exit (EXIT_FAILURE);
    }

    tmp = (int *) malloc (len * sizeof (int));
    if (tmp != NULL) {
        memset (tmp, 0, len * sizeof (int));
        return (tmp);
    } else {
        fprintf (stderr, "ERROR: Cannot allocate memory for array allocate_intmem().\n");
        exit (EXIT_FAILURE);
    }
}
