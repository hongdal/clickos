__author__ = 'zhizhong pan'

import os
import glob
import numpy as np
import threading
import Queue
import matplotlib as mpl
import matplotlib.pyplot as plt
import matplotlib.cm as cm
import operator as o
import pandas as pd

work_dir = os.getcwd()
q = Queue.Queue()

def do_stat(file_name, fold_path):
    path = os.path.join(fold_path, file_name)
    data_list = []
    with open(path, 'r') as data_file:
        for item in data_file:
            if len(item.strip()) > 0:
                try:
                    data_list.append(int(item))
                except ValueError:
                    pass

    ids, raw, p_size, rate, rules = file_name.split('.')[0].split('_')
    mean = np.mean(data_list)
    median = np.median(data_list)
    std = np.std(data_list)

    q.put([int(ids), p_size, rate, rules, mean, median, std])


def stat_all_file():
    fold_path = os.path.join(work_dir, 'mediadata')
    save_path = os.path.join(work_dir, 'statdata')
    threads = []
    for dir_entry in glob.glob(fold_path + '/*.numbers'):

        file_name = dir_entry.split('/')[-1]
        t = threading.Thread(target=do_stat, args=(file_name, fold_path))

        t.start()
        threads.append(t)

    for t in threads:
        t.join()

    result_data = []
    while not q.empty():
        result_data.append(q.get())

    return result_data
    # with open(save_path+'/statresult.csv', 'w+') as output_file:
    # output_file.write('ID,Packet Size(B),Rate(MB),Rules,Mean(Nanosecond),Median(Nanosecond),Std(Nanosecond)' + '\n')

    # output_file.write(str(q.get())[1:-1] + '\n')


def read_vm_file():
    file_path = os.path.join(work_dir, 'median_well')
    result_data = []
    with open(file_path, 'r') as data_file:
        for item in data_file:
            item_list = item.strip().split(',')
            result_data.append([item_list[0], item_list[1], float(item_list[2]),
                               float(item_list[3]), float(item_list[4])])
    return result_data

def bar_plot(axis, d_points, condition_col, category_col, data_col):

    d_points = np.array(d_points)

    # filter condition
    d_points = d_points[d_points[:, 1] == '1024B']
    d_points = d_points[(d_points[:, 3]).astype(int) <= 2100]

    conditions = [(c, np.mean(d_points[d_points[:, condition_col] == c][:, data_col].astype(float)))
                  for c in np.unique(d_points[:, condition_col])]
    categories = [(c, np.mean(d_points[d_points[:, category_col] == c][:, data_col].astype(float)))
                  for c in np.unique(d_points[:, category_col])]

    # print conditions

    # sort the conditions, categories and data so that the bars in
    # the plot will be ordered by category and condition

    conditions = [c[0] for c in conditions]
    categories = [c[0] for c in sorted(categories, key=o.itemgetter(1))]
    # print conditions

    d_points = np.array(sorted(d_points, key=lambda x: categories.index(x[category_col])))

    # the space between each set of bars
    space = 0.3
    n = len(conditions)
    width_unit = (1 - space) / (len(conditions))

    # Create a set of bars at each position
    for i, cond in enumerate(conditions):
        indeces = range(1, len(categories)+1)
        vals = d_points[d_points[:, condition_col] == cond][:, data_col].astype(np.float)
        vals /= 1000
        pos = [j - (1 - space) / 2. + i * width_unit for j in indeces]
        axis.bar(pos, vals, width=width_unit, label=cond[:-2] + ' Mb/s', color=cm.YlOrRd(float(i) / n))

    # Set the x-axis tick labels to be equal to the categories
    axis.set_xticks(indeces)
    axis.set_xticklabels(categories)
    plt.setp(plt.xticks()[1])

    # Add the axis labels
    axis.set_ylabel("Time (microseconds) ", fontsize=14)
    axis.set_xlabel("Number of Rules  in a Virtual Firewall", fontsize=14)

    # Add a legend
    handles, labels = axis.get_legend_handles_labels()
    axis.legend(handles[::-1], labels[::-1], loc='upper left', title='Throughput')


def gen_stat_image(result_data):
    mpl.rc('font', family='sans-serif')
    mpl.rc('font', serif='Helvetica Neue')
    fig = plt.figure(figsize=(12, 6))
    ax = fig.add_subplot(111)
    bar_plot(ax, result_data, 2, 3, 4)

    plt.show()
    fig.savefig("RULS_RATE_1024B" + ".pdf", format='pdf')

if __name__ == '__main__':
    data = stat_all_file()
    #data = read_vm_file()
    gen_stat_image(data)
