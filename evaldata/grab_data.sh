#!/bin/bash

if [[ $# -ne 2 ]]; then 
    echo "Usage: ./grab_data.sh <path_to_data_dir> <path_to_store_data>"
    echo "e.g. ./grab_data.sh ./rawdata/ ./aligndata"
    exit 1
fi

extention=".numbers"
src_dir=$1
dst_dir=$2
if [[ ! -d $src_dir ]]; then 
    echo "source directory not found: "$src_dir
    exit 1
fi
if [[ ! -d $dst_dir ]]; then 
    echo "mkdir: "$dst_dir
    mkdir $dst_dir
    if [[ $? -ne 0 ]]; then 
        exit 1
    fi
fi

files=$(ls $src_dir | grep -e '^[0-9]*_raw_[0-9]*B_[0-9]*MB_[0-9]*$')

for f in $files; do
    # delemitted by ','. First feild should be '@'; then 'number'; then '#'
    # We only care about the second feild. 
    awk -F ',' '{if ($1 ~ /^@/ && $2 ~ /^[0-9]*/ && $3 ~/^#/) print $2}' $src_dir"/"$f > $dst_dir$f$extention
done




