#!/bin/bash

ff=$(ps aux | grep 'xl destroy' | awk -F ' ' '{print $2}')

for f in $ff; do
    kill -9 $f
done

