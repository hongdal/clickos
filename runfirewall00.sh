#!/bin/bash

CURRENT_DIR=$(pwd) 

XEN_CONFIG=$CURRENT_DIR"/../clickos/minios/click0.xen"
xl create $XEN_CONFIG 

DOMID=$(xl list | grep click0 | awk -F' ' '{print $2}')
echo "Domain ID: "$DOMID

COSMOS_PATH=$CURRENT_DIR"/../cosmos/dist/bin/cosmos"
#CLICK_PATH=$CURRENT_DIR"/../clickos/minios/firewall00.click"
CLICK_PATH=$CURRENT_DIR"/../clickos/minios/firewall00.click"

$COSMOS_PATH start $DOMID $CLICK_PATH 

#/home/cosmos/dist/bin/cosmos start $DOMID /home/clickos/minios/firewall00.click

xl console $DOMID



