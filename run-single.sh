#!/bin/bash

if [[ $# -ne 2 ]]; then
	echo "Usage: ./run-single <config file number> <numberof rules>"
	echo "e.g. ./run-single 1 100"
	echo "e.g. ./run-single 0 900"
	exit 0
fi

ConfigFile="/home/clickos/minios/xens/click"$1".xen"
RuleFile="/home/clickos/minios/clicks/single/rule-"$2".click"

if [[ ! -e $ConfigFile ]]; then
	echo "Could not find Xen config file:"$ConfigFile
	exit 0
fi
if [[ ! -e $RuleFile ]]; then
	echo "Could not find rule file:"$RuleFile
	exit 0
fi

echo " >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>"
echo "Starting VM_"$1
xl create $ConfigFile
VMName="click"$1
DOMID=$(xl list | grep $VMName | awk -F' ' '{print $2}')
#echo "Domain ID: "$DOMID

echo " >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>"
echo "Loading rules from:"$RuleFile
/home/cosmos/dist/bin/cosmos start $DOMID $RuleFile 

echo " >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>"
/home/evaluation/start-time-test.sh 0


