#ifndef CLICK_NFV_FIREWALL_MATCH_HH
#define CLICK_NFV_FIREWALL_MATCH_HH

#include "initglobal.hh"
#include "firewalltable.hh"

CLICK_DECLS

class firewallmatch : public Element { 

public:
    firewallmatch():ft(g_ft), _active(0) {}
    ~firewallmatch(){} 

    const char *class_name() const		{ return "firewallmatch"; }
    const char *port_count() const		{ return "2/2"; }
    // input 0 and output 0 is PULL, others are PUSH.
    const char *processing() const      { return "lh/lh"; }
    void push(int, Packet*);
    Packet *pull(int);
    enum action domatch(Packet*); 


private:
    firewalltable* ft;
    char _active;
/**
 * These functions are for evaluation. 
 * To get rid of evaluation info, comment out this line.  
 **/
//#define FIREWALL_MATCH_EVALUATION 1
//#ifdef FIREWALL_MATCH_EVALUATION 
    long int fm_t_sec;
    long int fm_t_nsec;
    struct timespec fm_tvl;
//#endif

};



CLICK_ENDDECLS
#endif
