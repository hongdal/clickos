#include <click/config.h>
#include <stdio.h>
#include <stdlib.h>
#include "firewalltable.hh"

CLICK_DECLS
// comment this out when release. 
#define FIREWALLTABLE_EVAL 1
#define FIREWALLSTATE_EVAL 1

extern class firewalltable* g_ft;
const char* readable_state[8] = {
    "CLOSE___", 
    "SYN_1___", 
    "SYN_2___", 
    "EST_____", 
    "FIN_1___", 
    "FIN_2___", 
    "CLOSE_W_", 
    "LAST_ACK" 
};

/*******************************************************************/
// Definition of global functions. 
//
int getChainSize(struct ipchain* c)
{
	return c->size;
}

struct entry* allocEntry()
{
	entry *e = new entry();
	return e;
}

bool copyEntry(struct entry* e1, struct entry* e2)
{
	if (!(e1 && e2))
		return false;

	e1->src_ip = e2->src_ip;
	e1->src_ip_mask = e2->src_ip_mask;
	e1->des_ip = e2->des_ip;
	e1->des_ip_mask = e2->des_ip_mask;
	e1->src_port_min = e2->src_port_min;
	e1->src_port_max = e2->src_port_max;
	e1->des_port_min = e2->des_port_min;
	e1->des_port_max = e2->des_port_max;

	e1->protocol = e2->protocol;
	e1->action = e2->action;

	return true;

}

struct ipchain* allocChain()
{
	ipchain *c = new ipchain();
	struct entry* head = allocEntry();
	struct entry* tail = allocEntry();

	head->pre = NULL;
	head->next = tail;

	tail->pre = head;
	tail->next = NULL;

	c->head = head;
	c->tail = tail;
	c->size = 0;

	return c;
}


struct entry* getEntryByIndex(struct ipchain* c, int index)
{
	// TODO: can make a bit optimization!
	if (index > c->size - 1)
	{
		printf("ERROR: Index is out of bound");
		return NULL;
	}

	struct entry *cur = c->head->next;

	while(cur) {
		if (index == 0)
			return cur;
		else{
			cur = cur->next;
			index--;
		}
	}

	return NULL;
}

void printEntry(struct entry* e) 
{
	printf("-------------------------------------------------------------\n");
    printf("source ip          : %u\n", ntohl(e->src_ip));
    printf("source ip mask     : %u\n", ntohl(e->src_ip_mask));
    printf("destination ip     : %u\n", ntohl(e->des_ip));
    printf("destination ip mask: %u\n", ntohl(e->des_ip_mask));
    printf("srouce port        : %hu - %hu\n", e->src_port_min, e->src_port_max);
    printf("destination port   : %hu - %hu\n", e->des_port_min, e->des_port_max);
    printf("protocol           : %hhu\n", e->protocol);
    printf("action             : %d\n", int(e->action));
    printf("-------------------------------------------------------------\n");
}

bool addEntryAtTail(struct ipchain* c, struct entry* e)
{
    if (!(c && e))
		return false;

	struct entry* tmp = c->tail->pre;
	e->next = c->tail;
	e->pre = tmp;
	tmp->next = e;
	c->tail->pre = e;
	c->size++;

    return true;
}


bool addEntryAtHead(struct ipchain* c, struct entry* e)
{
    if (!(c && e))
		return false;

	struct entry* tmp = c->head->next;
	e->next = tmp;
	e->pre = c->head;
	tmp->pre = e;
	c->head->next = e;
	c->size++;

    return true;
}


bool delEntryByIndex(struct ipchain* c, int index)
{
	if (!c)
		return false;

	if (index > c->size - 1)
	{
		printf("ERROR: Index is out of bound");
		return false;
	}

	struct entry* tmp = getEntryByIndex(c, index);
	tmp->pre->next = tmp->next;
	tmp->next->pre = tmp->pre;
	free(tmp);
	return true;
}




void printChain(struct ipchain* c) {

	struct entry* en = c->head->next;
	while(en->next) {
		printEntry(en);
		en = en->next;
	}
}


inline struct entry* nextEntry(struct ipchain* c, struct entry* e){
	return e->next;
}


inline bool isEntryEqual(struct ipchain* c, struct entry* e1, struct entry* e2){
	if (e1->src_ip == e2->src_ip &&
		e1->src_ip_mask == e2->src_ip_mask &&
		e1->des_ip == e2->des_ip &&
		e1->des_ip_mask == e2->des_ip_mask &&
		e1->src_port_min == e2->src_port_min &&
		e1->src_port_max == e2->src_port_max &&
		e1->des_port_min == e2->des_port_min &&
		e1->des_port_max == e2->des_port_max &&
		e1->protocol == e2->protocol &&
		e1->action == e2->action)
		return true;
	else
		return false;
}


inline bool isEntryMatch(struct ipchain* c, struct entry* e1, struct entry* e2){
	//check ip address is or not matched
	if (e2->src_ip < e1->src_ip || (e2->src_ip > (e1->src_ip | e1->src_ip_mask)) ||
        e2->des_ip < e1->des_ip || (e2->des_ip > (e1->des_ip | e1->des_ip_mask)))
        return false;

	//check port
	if (e2->src_port_min < e1->src_port_min ||
		e2->src_port_max > e1->src_port_max ||
		e2->des_port_min < e1->des_port_min ||
		e2->des_port_max > e1->des_port_max)
		return false;

	// check protocol
	if (e1->protocol != e2->protocol)
		return false;
	
	return true;
}

/*******************************************************************/
// Definition of class firewallstate. 
//
firewallstate::firewallstate():
    _free(NULL),
    _use(NULL),
    // Note, firewallstate should be initiated after firewalltable. 
    _fwt(g_ft),
    _timer(this)
{
    int index = 0;
    struct state_entry* tmp;
    while ( index < (int)init_state_size ) {
        tmp = new state_entry();
        tmp->sib_pre = NULL;
        tmp->sib_next = NULL;
        tmp->rule = NULL;
        // append at the head of free link. 
        tmp->store_pre = NULL;
        tmp->store_next = _free;
        if ( NULL != _free ) {
            _free->store_pre = tmp;
        }
        _free = tmp;
        index++;
    }
}

firewallstate::~firewallstate() 
{
    struct state_entry* tmp;
    struct state_entry* kill;
    tmp = _use;
    while ( NULL != tmp ) {
        kill = tmp;
        tmp = tmp->store_next;
        delete kill;
    }
    tmp = _free;
    while ( NULL != tmp ) {
        kill = tmp;
        tmp = tmp->store_next;
        delete kill;
    }
}

int
firewallstate::initialize(ErrorHandler * errh) {
    // Initialize timer object
    _timer.initialize(this);
    // Set the timer to fire as soon as the router runs.
    _timer.schedule_now();

    return 0;
}

void 
firewallstate::run_timer(Timer *timer) {
    assert(timer == &_timer);
    // flush all timers. 
    fs_flush_timers();
    // do not use schedule_after_sec(), since that function 
    // may introduce imprecision in a cumulative way.
    _timer.reschedule_after_sec(timer_cycle);
}

int 
firewallstate::fs_flush_timers() {
    // update timers in each state entry and delete if necessarry.
    struct state_entry* tmp;
    struct state_entry* kill;
    int ret = 1;
    tmp = _use;
    while ( NULL != tmp ) {
        tmp->life_time--;
        if ( static_cast<signed int>(tmp->life_time) <= 0 ) {
            kill = tmp;
            tmp = tmp->store_next;
            kill->state = CLOSE;
            ret = fs_delete_entry(kill);
#ifdef FIREWALLSTATE_EVAL  
            fs_print_state(kill, NULL, 2);
#endif
        } else {
            tmp = tmp->store_next;
        }
    }
    return ret;
}

inline uint8_t 
firewallstate::fs_get_flag_by_packet(Packet* p) const {
    return static_cast<uint8_t>((p->tcp_header())->th_flags);
}


/***
 *  This function updates the state of a given state entry, 
 *  according to the given packet, including life_time. 
 *  
 *  If it's a valid, legitimate packet, then update the entry. 
 *  However, if it fails to update the entry (e.g., due to lack
 *  of memory), then this function may return 0. In this failed 
 *  case, the packet should be denied. 
 *
 *  If the given packet is not a legitimate or valid packet, 
 *  this function will return 0. In this failed case, the packet 
 *  should be denied. 
 *
 *  In summary, if this function returns 0, which means failure, 
 *  then the caller should interpreter it as 'deny' this packet. 
 *
 *  Input: 
 *      p   :   the packet to check against. 
 *      s   :   the state entry to be updated. 
 *  
 ***/
int
firewallstate::fs_update_state(Packet* p, struct state_entry* s) {
    // get tcp header pointer.  
    const struct click_tcp* thd = p->tcp_header();
    // look packet's flags. 
    uint8_t th_flags = thd->th_flags;
    // return value. default as successful.
    int ret = 1;
    // mask should be: 00010111
    uint8_t mask = (TH_FIN) | (TH_SYN) | (TH_ACK) | (TH_RST);
    
    switch ( th_flags & mask ) {
        // fin. 
        case 0x01: {
            // check. 
            if ( s->state == ESTABLISHED  ) {
                s->fin1_ack = ntohl(thd->th_seq);
                s->state = FIN_1;
                s->life_time = tcp_expiration;
            } else if ( s->state == FIN_1 ) {
                s->fin2_ack = ntohl(thd->th_seq);
                s->state = FIN_2;
                s->life_time = tcp_expiration;
            } else if ( s->state == CLOSING_WAIT ) {
                s->fin2_ack = ntohl(thd->th_seq); 
                s->state = LAST_ACK;
                s->life_time = syn_expiration;
            } else {
                // invalid fin. 
                ret = 0;
            }
            break;
        }
        // syn. 
        case 0x02: {
            // check. 
            if ( s->state == CLOSE ) {
                s->syn1_ack = ntohl(thd->th_seq);
                s->state = SYN_1;
                s->life_time = syn_expiration;
            } else {
                // invalid syn. 
                ret = 0;
            }
            break;
        }
        // ack. 
        case 0x10: {
            // check. 
            if ( s->state == ESTABLISHED ) {
                // pass this packet. 
                s->life_time = tcp_expiration;
            } else if ( s->state == SYN_2 && (ntohl(thd->th_ack) == s->syn2_ack+1) ) {
                s->state = ESTABLISHED;
                s->life_time = tcp_expiration;
            } else if ( s->state == FIN_1 ) {
                // This ack is for the first FIN.  
                if ( ntohl(thd->th_ack) == s->fin1_ack+1 ) {
                    s->state = CLOSING_WAIT;
                    s->life_time = tcp_expiration;
                }
            } else if ( s->state == FIN_2 ) {
                // This ack is for the second FIN. 
                if ( ntohl(thd->th_ack) == s->fin2_ack+1 ) {
                    s->state = LAST_ACK;
                    s->life_time = syn_expiration;
                }
            } else if ( s->state == LAST_ACK && (ntohl(thd->th_ack) == s->fin2_ack+1) ) {
                s->state = CLOSE;
                ret = fs_delete_entry(s);
            } else {
                // invalid ack. 
                ret = 0;
            }
            break;
        }
        // fin + ack 
        case 0x11: {
            // check. 
            if ( s->state == ESTABLISHED ) {
                s->fin1_ack = ntohl(thd->th_seq);
                s->state = FIN_1;
                s->life_time = tcp_expiration;
            } else if ( s->state == FIN_1 ) { 
                // reply to the previous fin. 
                if ( ntohl(thd->th_ack) == s->syn1_ack+1 ) {
                    s->state = LAST_ACK;
                    s->life_time = syn_expiration;
                // this ack reply to other data segments, not previous fin. 
                } else {
                    s->state = FIN_2; 
                    s->life_time = tcp_expiration;
                }
                s->fin2_ack = ntohl(thd->th_seq);
            // this case, ack must reply to a data segment. 
            } else if ( s->state == CLOSING_WAIT ) {
                s->state = LAST_ACK;
                s->fin2_ack = ntohl(thd->th_seq);
                s->life_time = syn_expiration;
            } else {
                // invalid fin + ack.
                ret = 0;
            }
            break;
        }
        // syn + ack 
        case 0x12: {
            // check. 
            if ( s->state == SYN_1 && (ntohl(thd->th_ack) == s->syn1_ack+1)) {
                s->syn2_ack = ntohl(thd->th_seq);
                s->state = SYN_2;
                s->life_time = syn_expiration;
            } else {
                // invalid syn+ack. 
                ret = 0;
            }
            break;
        }
        // rst (+ack)
        case 0x14: case 0x04: {
            // check. 
            s->state = CLOSE;
            // should delete this node.
            ret = fs_delete_entry(s);
            // reset this connection. 
            break;
        }
        // None of the above. 
        default: {
            // invalid tcp flag.  
            ret = 0;
        }
    }

    //
    // Here I print the latest state. 
    // Although, the state entry may have been 'deleted',
    // the content is still valid. 
    // If it's in multi-core context, this should be adjusted appropriately. 
    //
#ifdef FIREWALLSTATE_EVAL
    if ( ret > 0 ) {
        fs_print_state(s, p, 0);
    } else {
        fs_print_state(s, p, 1);
    }
#endif
    return ret;
}


/**
 *  This function adds a new state entry. 
 *  The new state entry is appended at the front of the link of 
 *  a given firewall rule (specified by parameter rule).
 *
 *  Only tcp syn packet can trigger this function. 
 *
 *  If it's not a valid syn, then fs_update_state(), which is called by this 
 *  function, will return a failure. The failure will prevent the state 
 *  entry from being appended to the 'use' link. Instead, the state entry will 
 *  be appended back to the 'free' link. 
 *
 *  p   :   raw packet. 
 *  e   :   entry that derives from p. 
 *  rule:   which rule introduce this state. 
 *
 **/
int 
firewallstate::fs_add_entry(Packet* p, struct entry* e, struct entry* rule) {
    struct state_entry *tmp = NULL; 
    // If free is not empty, grab a node from 'free'
    if ( NULL != _free ) {
        tmp = _free;
        _free = _free->store_next;
        if ( NULL != _free ) {
            _free->store_pre = NULL;
        }
    // free is empty. 
    } else {
        tmp = new state_entry(); 
        if ( NULL == tmp ) {
            fprintf(stderr, "Could not allocate state entry: out of memory!");
            return 0;
        }
    }
    // fill up new state node. 
    tmp->src_ip = e->src_ip;
    tmp->dst_ip = e->des_ip;
    tmp->src_port = e->src_port_min;
    tmp->dst_port = e->des_port_min;
    tmp->protocol = e->protocol;
    // initial state is set to CLOSE
    tmp->state = CLOSE;
    // Try to update the state of this state entry. 
    int ret = fs_update_state(p, tmp); 
    
    // If updateed successfully. 
    if ( 0 != ret ) {
        // append to 'use'
        tmp->store_pre = NULL;
        tmp->store_next = _use;
        if ( NULL != _use ) {
            _use->store_pre = tmp;
        }
        _use = tmp;
        tmp->rule = rule;
        tmp->sib_pre = NULL;
        tmp->sib_next = rule->connection;
        if ( NULL != rule->connection ) {
            rule->connection->sib_pre = tmp;
        }
    // Otherwise, append it back to 'free'
    } else {
        // append it to 'free' link. 
        tmp->rule = NULL;
        tmp->sib_pre = NULL;
        tmp->sib_next = NULL;
        tmp->store_pre = NULL;
        tmp->store_next = _free;
        if ( NULL != _free ) {
            _free->store_pre = tmp;
        }
        _free = tmp; 
    }
    return ret;
}

/**
 *  Delete a given state entry. 
 *  Delete from 'use' link and append to the head of 'free' link. 
 *  
 *  This function can be called by:
 *  'fs_update_state'. fs_update_state can delete a state entry
 *  due to the closure/reset of a connection. 
 *
 *  s   :   state entry to be deleted. 
 *
 * */
int
firewallstate::fs_delete_entry(struct state_entry* s) {
    // This node is not in use or an error may occur to this node,
    // It's illegal that a state entry is associated to none of the rules. 
    if ( NULL == s->rule ) {
        return 0;
    }
    // if it's the first node. update rule->connection. 
    if ( NULL == s->sib_pre ) {
        s->rule->connection = s->sib_next;
    // else it's not first node. update its previous node. 
    } else {
        s->sib_pre->sib_next = s->sib_next;
    }
    // if it's not the last node. update its next node. 
    if ( NULL != s->sib_next ) {
        s->sib_next->sib_pre = s->sib_pre;
    }
    // garuantee it move clear from rule links.    
    s->sib_pre = s->sib_next = NULL;

    // if it's the first one, the update _use. 
    if ( NULL == s->store_pre ) {
        _use = s->store_next;
    // else it's not the first one. update its previous node. 
    } else {
        s->store_pre->store_next = s->store_next;
    }
    // if it's not the last one, update its next node. 
    if ( NULL != s->store_next ) {
        s->store_next->store_pre = s->store_pre;
    }
    
    // Here we'd better tear it away from sib-link. 
    s->sib_pre = NULL;
    s->sib_next = NULL;
    s->rule = NULL;

    // append it to 'free' link. 
    s->store_pre = NULL;
    s->store_next = _free;
    if ( NULL != _free ) {
        _free->store_pre = s;
    }
    _free = s; 

    return 1;
}

/**
 *  This function checks whether there is a state entry that 
 *  matches the given packet. 
 *  If the state entry exists, then return a pointer to that entry. 
 *  Otherwise, return NULL.
 *
 ***/
struct state_entry*
firewallstate::fs_check_entry(Packet* p) const {
    const struct click_ip *iph = p->ip_header();
    const struct click_tcp *thd = p->tcp_header();
    uint32_t src_ip = (uint32_t)(iph->ip_src.s_addr);
    uint32_t dst_ip = (uint32_t)(iph->ip_dst.s_addr);
    uint16_t src_port = (uint16_t)ntohs(thd->th_sport);
    uint16_t dst_port = (uint16_t)ntohs(thd->th_dport);
    struct state_entry* s;
    s = _use;
    while ( NULL != s ) {
        if ( ((src_ip == s->src_ip)&& 
              (dst_ip == s->dst_ip)&&
              (src_port == s->src_port)&&
              (dst_port == s->dst_port)) || 
             ((src_ip == s->dst_ip)&& 
              (dst_ip == s->src_ip)&&
              (src_port == s->src_port)&&
              (dst_port == s->dst_port)) || 
             ((src_ip == s->src_ip)&& 
              (dst_ip == s->dst_ip)&&
              (src_port == s->dst_port)&&
              (dst_port == s->src_port)) || 
             ((src_ip == s->dst_ip)&& 
              (dst_ip == s->src_ip)&&
              (src_port == s->dst_port)&&
              (dst_port == s->src_port))
           ) {
               // found it. 
               return s;
           }
        s = s->store_next;
    }
    return NULL;
}


/**
 *  This function prints out the given state entry. 
 *  The main purpose of this function is for debugging. 
 *  When the traffic rate is high, the output of this print function 
 *  might make little sense. 
 *  --------------------------------------------------------------
 *  When employ this function in debugging, it is recommended to 
 *  make the traffic rate low enough, e.g., 10 packets per second. 
 *  --------------------------------------------------------------
 *
 *  s       :   pointer to the given state entry that will be printed. 
 *  p       :   pointer to the given packet that might potentially just trigger
 *              an update of state. 
 *  purpose :   0 - update failure, 1 - update success, 2 - none purpose. 
 * 
 *  The format of the print looks like: 
 *  
 *  192.168.1.1:1024 - 192.168.1.2:80 [SYN_1___] (s) [120+0]:[-] [-s--]
 *  192.168.1.2:80 - 192.168.1.1:1024 [SYN_2___] (s) [200+0]:[121] [-sa-]
 *  192.168.1.1:1024 - 192.168.1.2:80 [EST_____] (s) [121+1024]:[201] [--a-]
 *  192.168.1.1:1024 - 192.168.1.2:80 [FIN_1___] (s) [1146+10]:[201] [--af]
 *
 *  192.168.1.2:80 - 192.168.1.1:1024 [CLOSE_W_] (s) [201+0]:[1157] [--a-] 
 *  192.168.1.2:80 - 192.168.1.1:1024 [LAST_ACK] (s) [202+0]:[1157] [--af]
 *  192.168.1.1:1024 - 192.168.1.1:80 [CLOSE___] (s) [1157+0]:[203] [--a-]
 *
 *  Characters in the parentheses can be one of the following:
 *  s, f or -.
 *  (s) represents this packet's has successfully updated the state 
 *      entry's state. purpose=0. 
 *  (f) represents this packet's has failed to update the state entry's state.
 *      purpose=1.
 *  (-) represents no packet trigger this print, it's called by other reasons. 
 *      purpose=2.
 * 
 *  The first column indicates the packet's source ip.
 *  The second column indicates whether it's a packet. (since this function may be
 *  called by other reasons other than packet's trigger.)
 *  The third column indicates the packet's destination ip. 
 *  The fourth column indicates the state after the packet's update. 
 *  The fith column indicates whether this packet successully update. 
 *  The sixth column indicates [seq+len]:[ack_seq] 
 *  The seventh column indicates flags of the pacekts: [reset, syn, ack, fin]
 *
 *
 *  Note, for other reasons other than a packet's trigger, the second column 
 *  could be '+', which means the addresses are not a real packet's. Instead, 
 *  those ip/port addresses are observed from the state entry record. 
 *  If the second column indicates '-', then the addresses come from a real 
 *  packet that trigger an update which then calls this print function to print out 
 *  the latest state. 
 *
 *  If Pakcket is NULL, then the sixth and seventh column will not present. 
 *
 ***/
void 
firewallstate::fs_print_state(const struct state_entry* s, 
                              Packet *p = NULL, 
                              uint8_t purpose = 2 ) const 
{
    uint32_t src_ip;
    uint32_t dst_ip;
    uint16_t src_port;
    uint16_t dst_port;
    // Called by other reason. 
    if ( NULL == p ) {
        src_ip = static_cast<uint32_t>(ntohl(s->src_ip));
        dst_ip = static_cast<uint32_t>(ntohl(s->dst_ip));
        src_port = s->src_port;
        dst_port = s->dst_port;
        printf("%hhu.%hhu.%hhu.%hhu:%hu + %hhu.%hhu.%hhu.%hhu:%hu [%s] (-)\n",
                static_cast<uint8_t>((src_ip>>24) & 0x0ff),
                static_cast<uint8_t>((src_ip>>16) & 0x0ff),
                static_cast<uint8_t>((src_ip>> 8) & 0x0ff),
                static_cast<uint8_t>(src_ip       & 0x0ff), src_port,
                static_cast<uint8_t>((dst_ip>>24) & 0x0ff),
                static_cast<uint8_t>((dst_ip>>16) & 0x0ff),
                static_cast<uint8_t>((dst_ip>> 8) & 0x0ff),
                static_cast<uint8_t>(dst_ip       & 0x0ff), dst_port,
                readable_state[static_cast<int>(s->state)] 
              );
    } else {
        const struct click_ip *iph = p->ip_header();
        const struct click_tcp *thd = p->tcp_header();
        src_ip = static_cast<uint32_t>(ntohl(iph->ip_src.s_addr)); 
        dst_ip = static_cast<uint32_t>(ntohl(iph->ip_dst.s_addr)); 
        src_port = static_cast<uint16_t>(ntohs(thd->th_sport));
        dst_port = static_cast<uint16_t>(ntohs(thd->th_dport));
        uint32_t seq = static_cast<uint32_t>(ntohl(thd->th_seq));
        uint32_t ack = static_cast<uint32_t>(ntohl(thd->th_ack));
        printf("%hhu.%hhu.%hhu.%hhu:%hu - %hhu.%hhu.%hhu.%hhu:%hu [%s] (%s) <%lu>:<%lu>\n",
                static_cast<uint8_t>((src_ip>>24) & 0x0ff),
                static_cast<uint8_t>((src_ip>>16) & 0x0ff),
                static_cast<uint8_t>((src_ip>> 8) & 0x0ff),
                static_cast<uint8_t>(src_ip       & 0x0ff), src_port,
                static_cast<uint8_t>((dst_ip>>24) & 0x0ff),
                static_cast<uint8_t>((dst_ip>>16) & 0x0ff),
                static_cast<uint8_t>((dst_ip>> 8) & 0x0ff),
                static_cast<uint8_t>(dst_ip       & 0x0ff), dst_port,
                readable_state[static_cast<int>(s->state)],
                (0 == purpose) ? ("s") : (1==purpose ? "f" : "-"),
                seq, ack
              );
    }

}


/*******************************************************************/
// Definition of class firewalltable. 
//
firewalltable::firewalltable():
    ipt(allocChain())
{
    // Note, firewallstate is not initiated here. 
}

firewalltable::~firewalltable()
{	
	struct entry* tmp = ipt->head->next;
	while(tmp){
		free(tmp->pre);
		tmp = tmp->next;
	}

	free(ipt->tail);
	free(ipt);
}

bool 
firewalltable::ft_append_entry(struct entry* e_in)
{
    if ( NULL != e_in )
    {
    	struct entry* e = allocEntry();
    	if (!copyEntry(e, e_in))
    		return false;

    	return addEntryAtTail(ipt, e);
    }	
        
    else
    {
        printf("ERROR: Entry is NULL!");
        return false;
    }

}


bool 
firewalltable::ft_replace_entry(struct entry* e_in, int index)
{
	if (!e_in)
		return false;

	if (index > ipt->size - 1)
	{
		printf("ERROR: Index is out of bound");
		return false;
	}

	struct entry* e = allocEntry();
	if (!copyEntry(e, e_in))
		return false;

	struct entry* tmp = getEntryByIndex(ipt, index);	
	e->pre = tmp->pre;
	e->next = tmp->next;

	e->pre->next = e;
	e->next->pre = e;
	free(tmp);
	return true;
}

bool 
firewalltable::ft_insert_entry(struct entry* e_in, int index)
{
	if (!e_in)
		return false;

	if (index > ipt->size - 1)
	{
		printf("ERROR: Index is out of bound");
		return false;
	}

	struct entry* e = allocEntry();
	if (!copyEntry(e, e_in))
		return false;

	struct entry* tmp = getEntryByIndex(ipt, index);
	e->pre = tmp->pre;
	e->next = tmp;
	tmp->pre = e;
	e->pre->next = e;
	ipt->size++;

	return true;

}

bool 
firewalltable::ft_delete_entry(struct entry* e)
{
	struct entry* tmp = ipt->head->next;

	while(tmp->next)
	{
		if (isEntryMatch(ipt, e, tmp))
		{
			tmp->pre->next = tmp->next;
			tmp->next->pre = tmp->pre;
			free(tmp);
			ipt->size--;
			return true;
		}

		tmp = tmp->next;
	}

	return false;
}

bool 
firewalltable::ft_check_entry(struct entry* e)
{
	struct entry* tmp = ipt->head->next;
	while(tmp->next)
	{
		if (isEntryMatch(ipt, tmp, e))
			return true;

		tmp = tmp->next;
	}
	return false;
}


/***
 *  This function matches a given entry against the firewall rules. 
 *  It's for stateless firewall. 
 ***/
enum action 
firewalltable::ft_match_entry(struct entry* e)
{
	enum action ret = DROP;
	struct entry* tmp = ipt->head->next;
	while(tmp->next)
	{
		if (isEntryMatch(ipt, tmp, e))
			return tmp->action;

		tmp = tmp->next;
	}

	return ret;
}

/***
 *  This function matches a given entry against the firewall rules. 
 *  It's for stateful firewall. 
 ***/
enum action 
firewalltable::ft_match_entry(struct entry* e, Packet* p)
{
    uint8_t flags = fws->fs_get_flag_by_packet(p);
    // If it does not contain ONLY syn, then drop it. 
    if ( (0x00 == (TH_SYN & flags)) || (0x00 != (~(TH_SYN) & flags)) ) {
        return DROP;
    } 

    // Otherwise, might seek to add a new entry. 
	enum action ret = DROP;
    int result = 0;
	struct entry* tmp = ipt->head->next;
	while(tmp->next)
	{
        // Hit a match in the rule table.  
        if ( isEntryMatch(ipt, tmp, e) ) {
            // If this packet is accpeted.
            if ( DROP != tmp->action ) {
                // craete a new state entry.  
                result = fws->fs_add_entry(p, e, tmp);
                // If it fails to crate a new state entry. 
                if ( 0 == result ) {
                    return DROP;
                // Otherwise return whatever action except DROP.
                } else {
                    return tmp->action;
                }
            // Otherwise, not accpeted.
            } else {
                return DROP;
            }
        }
		tmp = tmp->next;
	}
    // If no hit, then return DROP. 
	return ret;
}


void 
firewalltable::ft_clear()
{
	struct entry* tmp;

	while(ipt->head->next != ipt->tail){
		tmp = ipt->head->next;
		ipt->head->next = tmp->next;
		tmp->next->pre = ipt->head;
		free(tmp);

	}

	ipt->size = 0;

}


void 
firewalltable::ft_print ()
{
	printChain(ipt);

}


CLICK_ENDDECLS
EXPORT_ELEMENT(firewalltable)
ELEMENT_MT_SAFE(firewalltable)
