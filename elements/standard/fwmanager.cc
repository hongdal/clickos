#include <click/config.h>
#include "fwmanager.hh"

CLICK_DECLS

// This feature is used for printing debug info. Comment out this 
// line to turn off the debug info. 
#define FWMANAGER_DEBUG_ON 1

/**
 *  struct rule. 
 *  This struct defines the format of rules that are transimtted over network.
 *  That is to say, firewall rule is in this formant when they are transmitted 
 *  over the network. 
 *
 *  A firewall rule must be converted to this format before it can be transmitted
 *  over network. Once a rule is received from the network by a recipient, it 
 *  must be firstly parsed using this data structure and then be converted into 
 *  an appropriate formant. E.g., the format of 'struct entry', which is defined
 *  in firewalltable.hh. 
 *  
 *  The size of this data structure determines how many rules can be transmitted
 *  over network in a time, by one IP packet. 
 *
 ***/
struct rule {
    uint32_t src_ip;
    uint32_t src_ip_mask;
    uint32_t dst_ip;
    uint32_t dst_ip_mask;
    uint16_t src_port_min;
    uint16_t src_port_max;
    uint16_t dst_port_min;
    uint16_t dst_port_max;
    uint8_t protocol;
    uint8_t action;
    uint16_t index; 
};

#define RULE_SIZE 28

enum OPTION_COMMANDS { C_APPEND, C_REPLACE, C_INSERT, C_DELETE, C_CHECK, C_CLEAR };
enum OPTION_DEBUG { D_PRINT };

bool
fwmanager::fm_append_entry(struct exentry* exe)
{
    bool ret = true;
    for (int i = 0; i < exe->entry_num; i++)
        ret &= ft->ft_append_entry(&exe->entry[i]);

    return ret;
}

bool 
fwmanager::fm_replace_entry(struct exentry* exe, uint16_t* indexes)
{
    bool ret = true;
#if 0    
    if (exe->entry_num != 1)
    {
        printf("Replace operation can accept only one entry once \n");
        return false;
    }    
#endif    
    for (int i = 0; i < exe->entry_num; i++){ 
        ret &= ft->ft_replace_entry(&exe->entry[i], (int)(indexes[i])); 
    }
    return ret;
}

/**
*   Note: if insert a set of entries at a time, 
*   the index may be changed after each insertion.  
***/
bool 
fwmanager::fm_insert_entry(struct exentry* exe, uint16_t* indexes)
{  
    if ( NULL == indexes ) {
        return false;
    }
    bool ret = true;
    for(int i = 0; i < exe->entry_num; i++)
        ret &= ft->ft_insert_entry(&exe->entry[i], (int)(indexes[i]));

    return ret;
}

bool
fwmanager::fm_delete_entry(struct exentry* exe)
{
    bool ret = true;
    for(int i = 0; i < exe->entry_num; i++)
        ret &= ft->ft_delete_entry(&exe->entry[i]);

    return ret;
}

bool
fwmanager::fm_check_entry(struct exentry* exe)
{
    bool ret = true;
    for(int i = 0; i < exe->entry_num; i++)
        ret &= ft->ft_check_entry(&exe->entry[i]);

    return ret;
}

void 
fwmanager::fm_clear()
{
    ft->ft_clear();
}

void 
fwmanager::fm_print()
{
    ft->ft_print();
}


/***
 * This function takes actions.
 * Every time we received a message packet, we need to 
 * go through this function.
 *
 * code         :  operation mode. 0->commands. 1->debug. 
 * option       :  options for commands. e.g. 0->append.   
 * ep           :  exentry pointer. It may store rules grabed from packets. 
 * indexes      :  this feild is for commands, like replace, insert, etc. 
 *
 * return 1 on success, 0 on failure. 
 *
 ****/
int  
fwmanager::fm_takeAction(uint8_t code, uint8_t option, struct exentry* ep, uint16_t* indexes)
{
    int ret = 1; 
    switch ( code ) {
        default : {
            fprintf(stderr, "takeAction: unknown mode!\n");
            goto BadAction;
        }
        /* sending commands */
        case 0 : {
            switch ( option ) {
                default : {
                    fprintf(stderr,"takeAction: unknow command option!\n");
                    goto BadAction;
                }
                /* append. */
                case C_APPEND : {
                    if ( fm_append_entry(ep) == 0 ) {
                        fprintf(stderr, "takeAction: append failed!\n");
                        goto BadAction;
                    }
                    break;
                }
                /* delete. */
                case C_DELETE : {
                    if ( fm_delete_entry(ep) == 0 ) {
                        fprintf(stderr, "takeActioin: delete failed!\n");
                        goto BadAction;
                    } 
                    break;
                }
                case C_INSERT : {
                    if ( fm_insert_entry(ep, indexes) == 0 ) {
                        fprintf(stderr, "takeActioin: insert failed!\n");
                        goto BadAction;
                    } 
                    break;
                }
                case C_REPLACE : {
                    if ( fm_replace_entry(ep, indexes) == 0 ) {
                        fprintf(stderr, "takeActioin: replace failed!\n");
                        goto BadAction;
                    } 
                    break; 
                }
                case C_CLEAR : {
                    fm_clear();
                    break; 
                }
            } 
            break;
        }
/**
 *  We are not going to use this piece of code. 
 *  The debug functionality is implemented in push()
 ****/
#if 0
        /* sending info. */
        case 1 : {
            switch ( option ) {
                default : {
                    fprintf(stderr, "takeAction: unknow debug option!\n");
                    goto BadAction;
                }
                /* print */
                case D_PRINT : {
                    /* do nothing */
                    break;
                }
            }
            break;
        }
#endif
    } /* end of switch of code. */

#ifdef FWMANAGER_DEBUG_ON 
    //fm_print();
    printf("RULE NUMBER: %d\n", ft->ft_get_size());
#endif
    return ret;

BadAction:
#ifdef FWMANAGER_DEBUG_ON 
    //fm_print();
    printf("RULE NUMBER: %d\n", ft->ft_get_size());
#endif
    ret = 0;
    return ret;
}



/* This function will be called when a packet that contains firewall 
*  rules is received. 
*  
*
***/
void 
fwmanager::push(int prot, Packet* p) 
{
    /* total number of rules in the payload. */
    uint16_t ruleCount = 0;
    /* current rule */
    uint16_t currentRule = 0;
    /* point to payload. */
    uint8_t *pdata = NULL;
    /* current offset of payload. */
    uint8_t *offset = NULL;
    struct rule r;
    const click_ip *iph = p->ip_header();
    int ip_len = ntohs(iph->ip_len);
    int payload_len = ip_len - (iph->ip_hl << 2);
    struct exentry *ep = NULL; 
    uint16_t *indexes = NULL;

    if ( 0xfd == iph->ip_p && payload_len > 0 ) {
        if ( NULL == (pdata = (uint8_t*)malloc(sizeof(uint8_t)*payload_len)) ) {
            fprintf(stderr, "WARNING: memory for payload failed to allocate.\n");
            goto BadEnd;
        }
        memcpy(pdata, (const uint8_t*)iph + (iph->ip_hl << 2), payload_len);
        
        switch ( pdata[0] ) 
        {
            default : {
                fprintf(stderr, "Could not parse mode. \n");
                goto BadEnd;
            }
            /* code is 0, it's command. */
            case 0 : {
                switch ( pdata[1] ) 
                {
                    default : {
                        fprintf(stderr, "Could not parse option. \n");
                        goto BadEnd;
                    }
                    /* insert a new rule. */
                    case C_INSERT : case C_REPLACE : {
                        ruleCount = ntohs(*(uint16_t*)(pdata+2));
                        indexes = (uint16_t*)malloc(sizeof(uint16_t)*ruleCount);
                        /* allocate exentry. */
                        ep = (struct exentry*)malloc(sizeof(struct exentry) + 
                                sizeof(struct entry)*ruleCount);
                        ep->length = sizeof(struct exentry) + sizeof(struct entry)*ruleCount;
                        ep->entry_num = ruleCount;
                        ep->elength = sizeof(struct entry)*ruleCount;
                        /* fulfill entries. */
                        offset = pdata+4; 
                        for ( currentRule = 0; currentRule < ruleCount; ++currentRule )
                        {
                            memcpy(&r, offset, RULE_SIZE);
                            /**
                             * There may be padding. Different machines may have 
                             * different paddings. So it may fail. But we don't 
                             * care about that in this version. 
                            ***/
                            offset += RULE_SIZE;
                            (ep->entry)[currentRule].src_ip = r.src_ip;
                            (ep->entry)[currentRule].src_ip_mask = r.src_ip_mask;
                            (ep->entry)[currentRule].des_ip = r.dst_ip;
                            (ep->entry)[currentRule].des_ip_mask = r.dst_ip_mask;
                            (ep->entry)[currentRule].protocol = r.protocol;
                            (ep->entry)[currentRule].src_port_min = ntohs(r.src_port_min);
                            (ep->entry)[currentRule].src_port_max = ntohs(r.src_port_max);
                            (ep->entry)[currentRule].des_port_min = ntohs(r.dst_port_min);
                            (ep->entry)[currentRule].des_port_max = ntohs(r.dst_port_max);
                            (ep->entry)[currentRule].action = (enum action)(r.action);
                            (ep->entry)[currentRule].connection = NULL;
                            indexes[currentRule] = (uint16_t)ntohs(r.index); 
                            /* recall that there is still a '\n' */
                            if ( '\n' != *offset ) {
                                fprintf(stderr, "ERROR: broken commands received.\n");
                                goto BadEnd;
                            }
                            offset++;
                        }
                        break;
                    }
                    /*append, delete*/
                    case C_APPEND : case C_DELETE : {
                        ruleCount = ntohs(*(uint16_t*)(pdata+2));
                        /* allocate exentry. */
                        ep = (struct exentry*)malloc(sizeof(struct exentry) + 
                                sizeof(struct entry)*ruleCount);
                        ep->length = sizeof(struct exentry) + sizeof(struct entry)*ruleCount;
                        ep->entry_num = ruleCount;
                        ep->elength = sizeof(struct entry)*ruleCount;
                        /* fulfill entries. */
                        offset = pdata+4; 
                        for ( currentRule = 0; currentRule < ruleCount; ++currentRule )
                        {
                            memcpy(&r, offset, RULE_SIZE);
                            /**
                             * There may be padding. Different machines may have 
                             * different paddings. So it may fail. But we don't 
                             * care about that in this version. 
                             *
                             * This is the only place that a struct entry 
                             * object is crated. 
                             *
                             * todo: 
                             *  abstract this operation, and define it as 
                             *  another function if possible. 
                            ***/
                            offset += RULE_SIZE;
                            (ep->entry)[currentRule].src_ip = r.src_ip;
                            (ep->entry)[currentRule].src_ip_mask = r.src_ip_mask;
                            (ep->entry)[currentRule].des_ip = r.dst_ip;
                            (ep->entry)[currentRule].des_ip_mask = r.dst_ip_mask;
                            (ep->entry)[currentRule].protocol = r.protocol;
                            (ep->entry)[currentRule].src_port_min = ntohs(r.src_port_min);
                            (ep->entry)[currentRule].src_port_max = ntohs(r.src_port_max);
                            (ep->entry)[currentRule].des_port_min = ntohs(r.dst_port_min);
                            (ep->entry)[currentRule].des_port_max = ntohs(r.dst_port_max);
                            (ep->entry)[currentRule].action = (enum action)(r.action);
                            (ep->entry)[currentRule].connection = NULL;
                            /* recall that there is still a '\n' */
                            if ( '\n' != *offset ) {
                                fprintf(stderr, "ERROR: broken commands received.\n");
                                goto BadEnd;
                            }
                            offset++;
                        }
                        break;
                    } /* end of append, delete */

                    /* clear */    
                    case C_CLEAR : {
                        break;
                    }
                } /* end of switch for option. */

                break;
            } /* end of sending commands. */
            /* sending debug info. */
            case 1 : {
                switch ( pdata[1] ) {
                    default : {
                        fprintf(stderr, "Could not parse debug info. \n");
                        goto BadEnd;
                    }
                    /* print table. */
                    case D_PRINT : {
                        // print table.
                        fm_print();
                        break;
                    } 
                }
                break;
            } /* end of sending debug info. */
        } /* end of switch for code. */

    } /* end of if. */

    /* Bad. */
    if ( fm_takeAction(pdata[0], pdata[1], ep, indexes) == 0 ) {
        p->kill();
    } else {
        output(0).push(p);
    }
    return;

BadEnd:
    if ( NULL != pdata ) {
        free(pdata);
    }
    if ( NULL != ep ) {
        free(ep);
    }
    p->kill();
    return;
}

CLICK_ENDDECLS
EXPORT_ELEMENT(fwmanager)
ELEMENT_MT_SAFE(fwmanager)
