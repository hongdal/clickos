#include <click/config.h>
#include "firewallmatch.hh"
#include "firewalltable.hh"

CLICK_DECLS

enum action 
firewallmatch::domatch(Packet* p)
{
    enum action ret = DROP;    
    const uint8_t *pdata; 
    const struct click_ip *iph = p->ip_header();
    //int ip_len = ntohs(iph->ip_len);
    //int payload_len = ip_len - (iph->ip_hl << 2);
    struct entry te;   
    
    /* First, make an entry from the packet. */
    te.action = DROP;      // This is not used.
    te.src_ip = (uint32_t)(iph->ip_src.s_addr);
    te.des_ip = (uint32_t)(iph->ip_dst.s_addr);
    te.src_ip_mask = 4294967295; // mask is 255.255.255.255
    te.des_ip_mask = 4294967295; // mask is 255.255.255.255

    te.protocol = (uint8_t)(iph->ip_p);
    switch ( te.protocol ) {
        default: {
            //printf("WARNNING: Unknow protocol for IP packet!\n");
            /* Fall through */
        }
        /* ICMP */
        case 1: {
            te.src_port_min = 0;
            te.src_port_max = 0;
            te.des_port_min = 0;
            te.des_port_max = 0;
            break;
        }
        /* TCP, UDP */
        case 6: case 17: {
            pdata = (uint8_t*)((uint8_t*)iph + (iph->ip_hl << 2)); 
            te.src_port_min = ntohs(*(uint16_t*)pdata);
            te.src_port_max = te.src_port_min;
            te.des_port_min = ntohs(*(uint16_t*)(pdata+2));
            te.des_port_max = te.des_port_min;
            break;
        }
    }

    // If it's an TCP packet, then apply state table. 
    if ( 6 == te.protocol ) {
        struct state_entry* se = ft->get_firewall_states()->fs_check_entry(p);
        // If this packet matches a state entry. 
        if ( NULL != se ) {
            // update state.
            int result = ft->get_firewall_states()->fs_update_state(p, se);
            if ( 0 != result ) {
                ret = ALLOW;
            }
        // If this packet could not match a state entry. 
        } else {
            // search firewall rules and add a new state entry if necessary. 
            ret = ft->ft_match_entry(&te, p);
        }
    // If it's not a TCP packet. (Now it's considered as UDP and ICMP) 
    } else {
        /* Then, compare this entry to each of the rule. */
        ret = ft->ft_match_entry(&te);
        
    }

    // printEntry(&te);

    // If there is no matched, then return DROP.
    return ret;
}


void
firewallmatch::push(int port, Packet *p)
{  
    // Port 1 is of type PUSH.
    // Flip the flag.
    _active = _active ^ 0x01;
    if ( _active ) {
        
    }
    p->kill();
}

Packet*
firewallmatch::pull(int port)
{
    if ( 0 == _active ) {
        return 0;
    }
    enum action res;
    // Grab packet from port 0. 
    Packet *p = input(0).pull();
    if ( 0 == p ) {
        return 0;
    }

//#ifdef FIREWALL_MATCH_EVALUATION
#if 0
    // Get time. 
    clock_gettime(CLOCK_REALTIME, &fm_tvl);
    fm_t_sec = fm_tvl.tv_sec;
    fm_t_nsec = fm_tvl.tv_nsec;
#endif
//#endif
    if ( (res = domatch(p)) == ALLOW ) {
//#ifdef FIREWALL_MATCH_EVALUATION
#if 0
        // Evaluate time. 
        clock_gettime(CLOCK_REALTIME, &fm_tvl);
        // This time won't too large. Few seconds. 
        fm_t_sec = (fm_tvl.tv_sec - fm_t_sec) * 1000000000;
        fm_t_nsec = fm_tvl.tv_nsec - fm_t_nsec + fm_t_sec;
        // Dump to file. 
        fprintf(stderr, "@,%ld,#\n", fm_t_nsec);
#endif
//#endif
        return p;
    } else if ( DROP == res ) {
        output(1).push(p);
        return 0;
    } else {
        //fprintf(stderr, "WARNNING: Unknow action!\n");
        p->kill();
        return 0;
    }
}

CLICK_ENDDECLS
EXPORT_ELEMENT(firewallmatch)
ELEMENT_MT_SAFE(firewallmatch)
