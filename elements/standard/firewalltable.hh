#ifndef CLICK_NFV_FIREWALL_TABLE_HH
#define CLICK_NFV_FIREWALL_TABLE_HH

/***
 *  This file defines firewall tables, including state table and 
 *  rule table.  
 *
 *  The state table now only supports TCP flows. 
 *  The state table is now under development. It does not track the 
 *  sequnce number of each side of a TCP connection. Therefore, it 
 *  is not able to fully support TCP connection state transfer. 
 *  Here is an example that the state table will fail to work. 
 *  
 *  Assume the topology: 
 *
 *  Host A  ----------  Firewall  ----------  Host B. 
 *           network1              network2
 *             (N1)                  (N2)
 *
 *  Packets may traval through N1 via different routing path, in which 
 *  case the packets may arrive at Firewall out of the order that they 
 *  are injected into N1.  
 *  There is a possibility that a packet with FIN set arrives prior to 
 *  a packet that was sent before the FIN-pacekt. Note that Firewall 
 *  does not maintain segments of TCP connections. Thus, there is no way 
 *  for Firewall to identify whether a packet arrives out of order or not. 
 *  This weakness leads disorder of TCP connection state transference.  
 *  Firewall considers a tcp connection will be torn down after receiving 
 *  FINs and thus tear down the corresponding state entry immediately. 
 *  However, there may be data packets in the flight.  
 *
 ****/

#include <click/element.hh>
#include <click/timer.hh>
#include <stdint.h>
#include <clicknet/tcp.h>

CLICK_DECLS

// initial number of nodes that will be allocated to the firewallstate. 
const uint32_t init_state_size = 10;
// after established, expiration = tcp_expiration * timer_cycle (3600 seconds)
// all states except CLOSE, SYN_1 and SYN_2
const uint32_t tcp_expiration = 900;
// during synchronization, expiration = syn_expiration * timer_cycle (120 seconds)
// SYN_1 and SYN_2. 
const uint32_t syn_expiration = 30;
// frequncy of timer rescheduling.
const uint32_t timer_cycle = 4;


enum action{ DROP, ALLOW };
// fw_state defines 8 states of stateful firewall.  
enum fw_state{ CLOSE, SYN_1, SYN_2, ESTABLISHED, FIN_1, FIN_2, CLOSING_WAIT, LAST_ACK };
extern const char* readable_state[8];

struct entry{
    unsigned int src_ip;
    unsigned int src_ip_mask;
    unsigned int des_ip;
    unsigned int des_ip_mask;
    
    uint16_t src_port_min;
    uint16_t src_port_max;
    uint16_t des_port_min;
    uint16_t des_port_max;

    unsigned char protocol;
    enum action action;

    struct entry* pre;
    struct entry* next;

    struct state_entry* connection;
};

struct ipchain{
    int size;
    struct entry* head;
    struct entry* tail;
};

struct state_entry {
    uint32_t src_ip;
    uint32_t dst_ip;
    uint16_t src_port;
    uint16_t dst_port;
    uint8_t protocol;

    enum fw_state state;
    uint32_t life_time;

    // record first syn seq number;
    uint32_t syn1_ack;
    // record second syn seq number;
    uint32_t syn2_ack;
    // record first fin seq number;
    uint32_t fin1_ack;
    // record second fin seq number;
    uint32_t fin2_ack;

    // links to next/previous connection nodes associated to the same rule. 
    struct state_entry* sib_pre;
    struct state_entry* sib_next;

    // links to next/previous connection nodes in free/use links. 
    struct state_entry* store_pre;
    struct state_entry* store_next;
   
    // indicats which rule introduce this state. 
    struct entry* rule;

    // If you need a search, then make a traverse of "use" link. 
    // "use" link can be implemented as a binary tree. However, now I don't
    // implement it that way. But it's easy to change into that way. 
};

void printEntry(struct entry *);

class firewallstate : public Element {
    public:
        firewallstate();
        ~firewallstate();
        
        const char* class_name() const      { return "firewallstate"; }
        const char* port_count() const      { return PORTS_0_0; }

        int initialize(ErrorHandler *errh);
        void run_timer(Timer *timer);

        // All return value: 0-fail, 1 successful.
        
        // periodically flush timers of all state entries.
        int fs_flush_timers();
        // make an state_entry according to Packet, and append this state_entry
        // to the given entry, a rule table entry.
        int fs_add_entry(Packet*, struct entry*, struct entry*);
        // delete a state_entry from 'use' link and put it into 'free' link. 
        int fs_delete_entry(struct state_entry*);
        // search all states and return a pointer to the entry that matches.
        struct state_entry *fs_check_entry(Packet*) const;

        // update a state entry, according to a given packet. 
        int fs_update_state(Packet*, struct state_entry*);
        inline uint8_t fs_get_flag_by_packet(Packet* p) const;
        
        // This function prints the state of a given state entry. 
        void fs_print_state(const struct state_entry*, Packet*, uint8_t) const;

    private:
        struct state_entry* _free;
        struct state_entry* _use;
        class firewalltable* _fwt;
        Timer _timer;
};


class firewalltable : public Element {
    public:
        firewalltable();
        ~firewalltable();

        const char *class_name() const      { return "firewalltable"; }
        const char *port_count() const      { return PORTS_0_0; }

        bool ft_append_entry(struct entry*);
        bool ft_replace_entry(struct entry*, int index);
        bool ft_insert_entry(struct entry*, int index);
        bool ft_delete_entry(struct entry*);
        bool ft_check_entry(struct entry*);
        enum action ft_match_entry(struct entry*); 
        // This function is for stateful firewall 
        enum action ft_match_entry(struct entry*, Packet*);
        void ft_clear();
        void ft_print();
    //        void ft_print_entry(unsigned int index);

        int ft_get_size() const { return ipt->size; }
        class firewallstate* get_firewall_states() const { return fws; }
        void set_firewall_states(class firewallstate* new_fws) { fws = new_fws; }

    private:
        struct ipchain* ipt;
        class firewallstate* fws;
};

CLICK_ENDDECLS
#endif
