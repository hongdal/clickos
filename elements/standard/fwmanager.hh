#ifndef CLICK_NFV_FWMANAGER_HH
#define CLICK_NFV_FWMANAGER_HH
#include <click/element.hh>
#include "firewalltable.hh"
#include "initglobal.hh"

CLICK_DECLS

/***
 *  Here we define an extentional entry structure.
 *  This structure will be used to store multiple rules. 
 *  This structure will be used in fwmanager element. 
 *
 *  Onece the element received a packet, then it will pasre the packet 
 *  and extracts the contents. The contetns in the packet will be the 
 *  commands and the rules. 
 *
 *  One packet might carry multipule rules.
 *  
 *  The packet carrying rules are sent from Dom0, which is considered as 
 *  the orchestration. So there should be a program that sends packets 
 *  running on Dom0. 
 *
 ***/
 struct exentry {
    int length;                 // Total length of the exentry  
    int elength;                // Length of entries;
    int entry_num;              // Number of entries;
    struct entry entry[0];      // Start of entries.  
     /* There may padding here. */
};


/**
 *  This is the class of firewall.
 *  This class take resposibility for filtering the packets that
 *  are passed to it.
 *
 * */
class fwmanager : public Element { 

public:
    fwmanager():ft(g_ft){}
    ~fwmanager(){} 

    const char *class_name() const		{ return "fwmanager"; }
    /* one input, one output*/
    const char *port_count() const		{ return PORTS_1_1; }
    // 'push' action will automatically call simple_action.
    // Packet *simple_action(Packet *);
    void push(int, Packet*);

    bool fm_append_entry(struct exentry*);
    bool fm_replace_entry(struct exentry*, uint16_t* indexes); 
    bool fm_insert_entry(struct exentry*, uint16_t* indexes);
    bool fm_delete_entry(struct exentry*);
    bool fm_check_entry(struct exentry*);
    int fm_takeAction(uint8_t, uint8_t, struct exentry*, uint16_t* indexes); 
    
    void fm_clear();
    void fm_print();


private:
    firewalltable* ft;

};

CLICK_ENDDECLS
#endif
