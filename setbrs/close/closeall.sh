#!/bin/bash
#   This script closes all xenbrs.
#
#   xenbr00~03
#   xenbr10~13
#   xenbr20~23
#   ...
#   
#   This first digits following xenbr is the index for vm instance.
#   If you want to kill 0~5, totally six vms, you need to specify the number by six.
#

xenbr="xenbr"
vmn=$1
# Four network cards for each vm.
nic=4
counter=0

echo ">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>"
while [[ $counter -lt $vmn ]]; do 
    index=0
    while [[ $index -lt $nic ]]; do 
        ifconfig $xenbr$counter$index down
        brctl delbr $xenbr$counter$index
        let "index=$index+1"
    done
    let "counter=$counter+1"
done

brctl show
echo ">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>"
echo "SHUTDOWN COMPLETED!"
exit 0



