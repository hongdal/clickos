#!/bin/bash

if [[ $# -ne 1 ]]; then
    echo "Usage: ./setxenbrs.sh <number of vms> "
    echo "e.g.  ./setxenbrs.sh 2"
    exit 0
fi


# start from 172.16.97.80, each vm would be allocated with a new ip. 
ipbase="172.16.97."
ipstart=80
ipextend=0
subs=0

xenbr="xenbr"
counter=0
index=0
vmn=$1
nic=4
brgname=$xenbr$counter$index
defaultgw="172.16.97.2"

while [[ $counter -lt $vmn ]]; do
    index=0
    while [[ $index -lt $nic ]]; do 
        # form the ip address.
        let "subs=$counter*$nic+$index"
        let "ipextend=$ipstart-$subs" 
        # form the xenbr name.
        brgname=$xenbr$counter$index
        echo $brgname" -> "$ipbase$ipextend
        
        brctl addbr $brgname
        ifconfig $brgname up
        ifconfig $brgname $ipbase$ipextend 
        #route add default gw $defaultgw $brgname 
        
        let "index=$index+1"
    done
    let "counter=$counter+1"
done

echo ">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>"
brctl show
echo ">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>"
echo "SET UP COMPLETED!"


